﻿using System.Collections.Generic;
using OL;
using UnityEngine;
using UnityEngine.UI;

namespace DivideEtImpera
{
    public class GridManager : MonoBehaviour
    {
        public List<Person> AllGridElements;
        
        public static GridManager I;

        void Awake()
        {
            I = this;
            AllGridElements = new List<Person>();
        }
        
        void Start()
        {
            
        }

        public int UpdateGrid()
        {
            List<NetworkBubble> bubbles = new List<NetworkBubble>();
            foreach (var person in GridManager.I.AllGridElements)
            {
                UpdateInDirection(person, Person.Direction.NORTH, TT.FirstImage, false);
                UpdateInDirection(person, Person.Direction.EAST, TT.ThirdImage, true);
                UpdateInDirection(person, Person.Direction.SOUTH, TT.FourthImage, false);
                UpdateInDirection(person, Person.Direction.WEST, TT.SecondImage, true);

                NetworkMember node = person.Myself;
                NetworkBubble ownBubble = null;
                foreach (NetworkBubble bubble in bubbles)
                {
                    if (bubble.MustInclude(node))
                    {
                        bubble.Include(node);
                        if (ownBubble == null)
                        {
                            ownBubble = bubble;
                        }
                        else
                        {
                            ownBubble.Engulf(bubble);
                        }
                    }
                }
                if (ownBubble == null)
                {
                    bubbles.Add(new NetworkBubble(node));
                }
            }

            bubbles.RemoveAll((x) => x.Empty);
            NetworkBubble largestBubble = null;
            foreach (NetworkBubble bubble in bubbles)
            {
                if ((largestBubble == null) || (bubble.Size > largestBubble.Size))
                {
                    largestBubble = bubble;
                }
            }
            Debug.Log($"Largest of {bubbles.Count} bubbles has {largestBubble.Size} members");
            foreach (NetworkBubble bubble in bubbles)
            {
                if (bubble == largestBubble)
                {
                    bubble.PaintAsLargest();
                }
                else
                {
                    bubble.PaintAsSmaller();
                }
            }
            return largestBubble.Size;
        }

        void UpdateInDirection(Person person, Person.Direction direction, string atag, bool changeY)
        {
            var gameParams = DEIManager.I().Params;
            
            var n = person.GetNeighbour(direction);
            if (n != null)
            {
                var relImage =
                    TT.FindGameObjectChildWithTag(person.gameObject, atag).GetComponent<Image>();
                
                var nrN = person.Myself.NeighbourRelationships[direction];

                var relPerc = nrN.Intensity / gameParams.InitialRelationshipMax;

                relImage.name = relPerc.ToString("F");

                DEIManager.I().AdjustRelationshipThickness(relImage, relPerc, changeY);
            }
        }


    }
}
