﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DivideEtImpera
{
    public class PlaySession
    {
        public PostManager.PostingPhase Phase;

        public Person PickedPerson;
        public PostableContent PickedContentData;

        public int LargestBubbleSize = int.MaxValue;

        public List<PostedContents> AllPostedContents;

        public PlaySession()
        {
            AllPostedContents = new List<PostedContents>();
        }

        public void CurrentContentIsConfirmed()
        {
            PostedContents pc = new PostedContents();
            pc.SelectedGroup = PickedPerson.SelectionGroup;
            pc.Content = PickedContentData;
            AllPostedContents.Add(pc);
        }
    }
}
