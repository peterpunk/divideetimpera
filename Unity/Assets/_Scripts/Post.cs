﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.DeExtensions;
using DG.Tweening;
using TMPro;
using System;

public class Post : MonoBehaviour
{
    public GameObject newPostBadge;
    public TextMeshProUGUI text;

    static readonly int NewPostPersistenceTime = 10;
    static readonly int NewPostFadeTime = 3;

    void Start()
    {
        newPostBadge.SetActive(true);
        newPostBadge.GetComponent<Image>()
            .DOFade(0, NewPostFadeTime).SetDelay(NewPostPersistenceTime);
        newPostBadge.GetComponentInChildren<TextMeshProUGUI>()
            .DOFade(0, NewPostFadeTime).SetDelay(NewPostPersistenceTime);
    }

    public void SetText(string text)
    {
        this.text.text = text;
    }
}
