﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.DeExtensions;
using DG.Tweening;
using TMPro;
using System;

public class Tip : MonoBehaviour
{
    public TextMeshProUGUI text;
    
    public void SetText(string text)
    {
        this.text.text = text;
    }
}
