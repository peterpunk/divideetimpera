﻿using System.Collections;
using System.Collections.Generic;
using DG.DeExtensions;
using DG.Tweening;
using OL;
using UnityEngine;
using UnityEngine.UI;

namespace DivideEtImpera
{
    public class IntroManager : MonoBehaviour
    {
        public Image InnerCover;
        public CanvasGroup Info;
        public GameObject LanguageChoice;
        public GameObject Intro;
        public GameObject SettingsButton;

        void Start()
        {            
            PrepareLanguageChoiceScreen(true);
            InnerCover.SetAlpha(1);
            DOVirtual.DelayedCall(4, () => InnerCover.DOFade(0, 1));
        }

        public void Play()
        {
            SceneChanger.ChangeScene("SocialNetwork");
        }

        public void LearnMore(string anUrl)
        {
            Application.OpenURL(anUrl);
        }

        public void OpenInfo()
        {
            if (Info.alpha > .9f)
            {
                CloseInfo();
                return;
            }
            Info.alpha = 0;
            Info.gameObject.SetActive(true);
            Info.DOFade(1, 0.5f);
        }

        public void CloseInfo()
        {
            Info.DOFade(0, 0.5f).OnComplete(() =>
            {
                Info.gameObject.SetActive(false);
            });
        }

        private void PrepareLanguageChoiceScreen(bool languageChoice)
        {
            LanguageChoice.SetActive(languageChoice);
            Intro.SetActive(!languageChoice);
            SettingsButton.SetActive(!languageChoice);
        }

        public void ChooseEnglish()
        {
            ChooseLanguage(Language.En);
        }

        public void ChooseItalian()
        {
            ChooseLanguage(Language.It);
        }

        private void ChooseLanguage(Language language)
        {
            Debug.Log($"Switching to {language}");
            SimpleI18n.Language = language;
            PrepareLanguageChoiceScreen(false);
        }

    }
}
