﻿using DG.Debugging;
using DG.DeExtensions;
using OL;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Direction = DivideEtImpera.Person.Direction;

namespace DivideEtImpera
{
    public class DEIManager : SimpleGameManager
    {
        public List<PostableContent> AllPossibleContents;        
        public List<NetworkMember> AllMembers;
        public Dictionary<Language, List<string>> AllTips;

        public AssetConfigs Assets;
        public GameParams Params;

        public Setuppable ContentSetup;
        public Setuppable MembersSetup;
        public Setuppable TipsSetup;
        public Setuppable WorldSetup;        

        private List<PostableContent> FreshContents = new List<PostableContent>();

        public override void SetConcreteInstance()
        {
            InjectedReferrableInstance = this;

            ContentSetup = Setuppable.NewInstance();
            MembersSetup = Setuppable.NewInstance();
            TipsSetup = Setuppable.NewInstance();
            WorldSetup = Setuppable.NewInstance();
            
            ReferenceMasterAudioVolume = 1;
        }

        public static DEIManager I()
        {
            return (DEIManager)InjectedReferrableInstance;
        }

        public override void OnStart()
        {
            QualitySettings.vSyncCount = 0; // VSync must be disabled
            Application.targetFrameRate = 40;
        }

        public PostableContent RandomFreshContent()
        {
            if (FreshContents.Count == 0)
            {
                FreshContents.AddRange(AllPossibleContents);
            }

            PostableContent content = TT.OneRnd<PostableContent>(FreshContents);
            FreshContents.Remove(content);
            content.Text = TT.OneRnd<string>(content.LocalisedVariants);
            return content;
        }

        void Update()
        {
            if (FrameworkAndPlayState.IsRunning())
                return;

            if (ContentSetup.IsToBeSetupped())
            {
                ContentSetup.Setupping();

                AllPossibleContents = new List<PostableContent>();

                if (!Production)
                {
                    StartCoroutine(TT.DownloadCSVCoroutine("1pxFcsSDObwEXJeWGkpBOC6LqNaGF7Urm2OjAI5UQyWM",
                        (csv) =>
                        {
                            ContentAndCharactersImporter.ImportContent(AllPossibleContents, csv);
                            ContentSetup.SetupCompleted();
                        }, true, "Contents", "0"));
                }
                else
                {
                    var data = Resources.Load("Contents") as TextAsset;
                    ContentAndCharactersImporter.ImportContent(AllPossibleContents, data.text);
                    ContentSetup.SetupCompleted();
                }
            }

            if (ContentSetup.IsSetupping())
                return;

            if (MembersSetup.IsToBeSetupped())
            {
                MembersSetup.Setupping();

                AllMembers = new List<NetworkMember>();

                if (!Production)
                {
                    StartCoroutine(TT.DownloadCSVCoroutine("1pxFcsSDObwEXJeWGkpBOC6LqNaGF7Urm2OjAI5UQyWM",
                        (csv) =>
                        {
                            ContentAndCharactersImporter.ImportMembers(AllMembers, csv);
                            MembersSetup.SetupCompleted();
                        }, true, "Members", "984721289"));
                }
                else
                {
                    var data = Resources.Load("Members") as TextAsset;
                    ContentAndCharactersImporter.ImportMembers(AllMembers, data.text);
                    MembersSetup.SetupCompleted();
                }
            }

            if (MembersSetup.IsSetupping())
                return;

            if (TipsSetup.IsToBeSetupped())
            {
                TipsSetup.Setupping();

                AllTips = new Dictionary<Language, List<string>>();
                foreach (Language lang in SimpleI18n.AllLanguages)
                {
                    AllTips[lang] = new List<string>();
                }                

                if (!Production)
                {
                    StartCoroutine(TT.DownloadCSVCoroutine("1pxFcsSDObwEXJeWGkpBOC6LqNaGF7Urm2OjAI5UQyWM",
                        (csv) =>
                        {
                            ContentAndCharactersImporter.ImportTips(AllTips, csv);
                            TipsSetup.SetupCompleted();
                        }, true, "Tips", "831507527"));
                }
                else
                {
                    var data = Resources.Load("Tips") as TextAsset;
                    ContentAndCharactersImporter.ImportTips(AllTips, data.text);
                    TipsSetup.SetupCompleted();
                }              
            }

            if (MembersSetup.IsSetupping())
                return;

            if (WorldSetup.IsToBeSetupped())
            {
                WorldSetup.SetupCompleted();

                TT.Shuffle(AllMembers);

                int i = 0;
                foreach (var gridPerson in GridManager.I.AllGridElements)
                {
                    gridPerson.Setup(AllMembers[i]);
                    i++;
                }

                foreach (var gridPerson in GridManager.I.AllGridElements)
                {
                    if (gridPerson.Column < 4)
                    {
                        var personEast = Person.GetPersonAtGrid(gridPerson.Row, gridPerson.Column + 1);
                        gridPerson.Neighbours[Direction.EAST] = personEast;

                        //lazy
                        if (!gridPerson.Myself.NeighbourRelationships.ContainsKey(Direction.EAST))
                        {
                            CreateRelationship(gridPerson, personEast, Direction.EAST, Direction.WEST);
                        }
                    }

                    if (gridPerson.Column > 0)
                    {
                        var personWest = Person.GetPersonAtGrid(gridPerson.Row, gridPerson.Column - 1);
                        gridPerson.Neighbours[Direction.WEST] = personWest;
                        if (!gridPerson.Myself.NeighbourRelationships.ContainsKey(Direction.WEST))
                        {
                            CreateRelationship(gridPerson, personWest, Direction.WEST, Direction.EAST);
                        }
                    }

                    if (gridPerson.Row < 4)
                    {
                        var personSouth = Person.GetPersonAtGrid(gridPerson.Row + 1, gridPerson.Column);
                        gridPerson.Neighbours[Direction.SOUTH] = personSouth;
                        if (!gridPerson.Myself.NeighbourRelationships.ContainsKey(Direction.SOUTH))
                        {
                            CreateRelationship(gridPerson, personSouth, Direction.SOUTH, Direction.NORTH);
                        }
                    }

                    if (gridPerson.Row > 0)
                    {
                        var personNorth = Person.GetPersonAtGrid(gridPerson.Row - 1, gridPerson.Column);
                        gridPerson.Neighbours[Direction.NORTH] = personNorth;
                        if (!gridPerson.Myself.NeighbourRelationships.ContainsKey(Direction.NORTH))
                        {
                            CreateRelationship(gridPerson, personNorth, Direction.NORTH, Direction.SOUTH);
                        }
                    }
                }

                GridManager.I.UpdateGrid();

                FrameworkAndPlayState.SetupCompleted();
            }
            
            if (FrameworkAndPlayState.IsSetupped() &&
                !FrameworkAndPlayState.IsPaused() &&
                !FrameworkAndPlayState.IsRunning())
            {
                SetPlayStateToRunAndFirstStartTime();
            }
        }

        internal void AdjustRelationshipThickness(Image lineImage, float relationshipPercentage, bool horizontalLine)
        {
            var newThick = Params.MaxThicknessRelInDetail * relationshipPercentage;
            if (newThick < 1 && newThick > 0)
            {
                newThick = 1;
            }
            if (horizontalLine)
            {
                lineImage.rectTransform.sizeDelta = new Vector2(lineImage.rectTransform.sizeDelta.x, newThick);
            }
            else
            {
                lineImage.rectTransform.sizeDelta = new Vector2(newThick, lineImage.rectTransform.sizeDelta.y);
            }
            if (relationshipPercentage < 0.1f && relationshipPercentage > 0)
            {
                lineImage.SetAlpha(0.1f);
            }
            else
            {
                lineImage.SetAlpha(relationshipPercentage);
            }
        }

        static void CreateRelationship(Person source, Person to,
            Direction toDirection, Direction toOppositeDirection)
        {
            if (to != null)
            {

                int intensityOverFive = Mathf.RoundToInt(UnityEngine.Random.Range(I().Params.InitialRelationshipMin, I().Params.InitialRelationshipMax) / 5);
                float intensity = intensityOverFive * 5;
                NetworkRelationship nr = new NetworkRelationship(source.Myself, to.Myself, intensity);
                
                source.Myself.NeighbourRelationships[toDirection] = nr;
                to.Myself.NeighbourRelationships[toOppositeDirection] = nr;
            }
            else
            {
                Delogger.Log($"{source} to",$"null? {source.Row} {source.Column}");
            }
        }

        public override void SetPlayStateToRunAndFirstStartTime()
        {
            DEITimeManager.I().StartCounting(0);
            FrameworkAndPlayState.Run();
        }
    }
}