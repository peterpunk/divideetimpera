﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DivideEtImpera
{
    public class PostedContents
    {
        public List<Person> SelectedGroup;
        public PostableContent Content;
    }
}
