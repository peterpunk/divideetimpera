﻿using DG.Debugging;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DivideEtImpera
{
    public class NetworkRelationship
    {
        private List<NetworkMember> Members = new List<NetworkMember>(2); // must be only two

        // 0 - 100?
        public float Intensity;

        public NetworkRelationship(NetworkMember one, NetworkMember two, float intensity)
        {
            Members.Add(one);
            Members.Add(two);
            this.Intensity = intensity;
        }

        public float With(NetworkMember member)
        {
            if (Members.Contains(member))
            {
                // Delogger.Log(This(), "Intensity: {Intensity}");
                return Intensity;
            }
            // Delogger.Log(This(), "Intensity: 0 (not neighbours)");
            return 0f;
        }

        private string This()
        {
            return $"NetworkRelationship {Members[0].Name}-{Members[1].Name}";
        }
    }
}
