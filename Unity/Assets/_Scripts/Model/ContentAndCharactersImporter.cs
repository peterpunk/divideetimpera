﻿using System.Collections;
using System.Collections.Generic;
using OL;
using UnityEngine;
using System;

namespace DivideEtImpera
{
    public static class ContentAndCharactersImporter
    {
        public static void ImportContent(List<PostableContent> allPossibleContents, string csv)
        {
            List<List<string>> rawData = TT.ParseCSV(csv);

            foreach (var alist in rawData)
            {
                int i = 0;
                var text = alist[i];
                var theme = alist[++i];
                var subtheme = alist[++i];
                var palatability = alist[++i];
                var palatableTo = alist[++i];
                var strength = alist[++i];
                var offensiveTo = alist[++i];
                var synonym = alist[++i];
                var itText = alist[++i];
                var itSynonym = alist[++i];

                if (TT.IsNullOrWhitespace(text.Trim()) || TT.IsNullOrWhitespace(theme.Trim()))
                    continue;

                try
                {
                    var postableContent = new PostableContent
                    {
                        Theme = theme,
                        Subtheme = subtheme,
                        Palatability = int.Parse(palatability),
                        PalatableTo = palatableTo,
                        Strength = int.Parse(strength),
                        OffensiveTo = offensiveTo,
                    };
                    postableContent.InitialiseVariants();
                    if (text.Length > 0)
                    {
                        postableContent.Variants[Language.En].Add(text);
                    }
                    if (synonym.Length > 0)
                    {
                        postableContent.Variants[Language.En].Add(synonym);
                    }
                    if (itText.Length > 0)
                    {
                        postableContent.Variants[Language.It].Add(itText);
                    }
                    if (itSynonym.Length > 0)
                    {
                        postableContent.Variants[Language.It].Add(itSynonym);
                    }
                    if (postableContent.Variants[Language.It].Count == 0)
                    {
                        // Use English as default
                        postableContent.Variants[Language.It].AddRange(postableContent.Variants[Language.En]);
                    }
                    allPossibleContents.Add(postableContent);
                }
                catch (FormatException fe)
                {
                    Debug.LogWarning($"Incomplete record: skipped «{text}» content");
                    Debug.LogException(fe);
                }
            }
        }

        public static void ImportMembers(List<NetworkMember> allMembers, string csv)
        {
            List<List<string>> rawData = TT.ParseCSV(csv);

            foreach (var alist in rawData)
            {
                int i = 0;
                var name = alist[i];
                var gender = alist[++i];
                var race = alist[++i];
                var creed = alist[++i];
                var age = alist[++i];
                var wealth = alist[++i];
                var sexuality = alist[++i];
                var citizenship = alist[++i];

                if (TT.IsNullOrWhitespace(name.Trim()) || TT.IsNullOrWhitespace(gender.Trim()))
                    continue;

                var nm = new NetworkMember
                {
                    Name = name,
                    Gender = gender,
                    Race = race,
                    Creed = creed,
                    Age = age,
                    Wealth = wealth,
                    Sexuality = sexuality,
                    Citizenship = citizenship,
                };
                nm.Setup();

                allMembers.Add(nm);
            }
        }

        public static void ImportTips(Dictionary<Language, List<string>> allTips, string csv)
        {
            List<List<string>> rawData = TT.ParseCSV(csv);

            foreach (var alist in rawData)
            {
                int i = 0;
                var text = alist[i];
                var itText = alist[++i];

                if (!TT.IsNullOrWhitespace(text.Trim()))
                {
                    allTips[Language.En].Add(text);
                }
                if (!TT.IsNullOrWhitespace(itText.Trim()))
                {
                    allTips[Language.It].Add(itText);
                }
            }

            foreach (Language lang in SimpleI18n.AllLanguages)
            {
                List<string> tips = allTips[lang];
                // Shuffle randomly one third of tips
                int shuffles = tips.Count / 3;
                int minShufflable = 3; // 1st and 2nd tips are essential there
                for (int i = 0; i < shuffles; i++)
                {
                    int a = UnityEngine.Random.Range(minShufflable, tips.Count);
                    int b = UnityEngine.Random.Range(minShufflable, tips.Count);
                    string tmp = tips[a];
                    tips[a] = tips[b];
                    tips[b] = tmp;
                }
                Debug.Log($"Tips in {lang} shuffled {shuffles} times");
            }
            
        }
    }
}