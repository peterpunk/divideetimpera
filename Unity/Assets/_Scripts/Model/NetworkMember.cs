using DG.Debugging;
using OL;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Direction = DivideEtImpera.Person.Direction;

namespace DivideEtImpera
{
    public class NetworkMember
    {
        // CONFIG

        public string Name;
        public string Gender;
        public string Race;
        public string Creed;
        public string Age;
        public string Wealth;
        public string Sexuality;
        public string Citizenship;

        // RELATIONSHIPS

        float engagement; // to the villains' hateful content

        public Dictionary<Direction, NetworkRelationship> NeighbourRelationships;


        // UI

        public Person GridPerson;


        // RUNTIME

        public List<Experience> Experiences = new List<Experience>();
        Experience CurrentExperience;

        public float EngagementVariation()
        {
            return (engagement / 100) - 1f;
        }

        string Describe(string tag)
        {
            switch (tag.ToLowerInvariant())
            {
                case "m":
                    switch (SimpleI18n.Language)
                    {
                        case Language.It:
                            return "un uomo";

                        default:
                            return "a man";
                    }                    
                case "f":
                    switch (SimpleI18n.Language)
                    {
                        case Language.It:
                            return "una donna";

                        default:
                            return "a woman";
                    }
                    
                case "n":
                    switch (SimpleI18n.Language)
                    {
                        case Language.It:
                            return "una persona non-binaria";

                        default:
                            return "a non-binary person";
                    }
                case "latino":
                    switch (SimpleI18n.Language)
                    {
                        case Language.It:
                            return (Gender == "F" ? "latinoamericana" : "latinoamericano");

                        default:
                            return (Gender == "F" ? "Latina" : "Latino");
                    }

                case "straight":
                    switch (SimpleI18n.Language)
                    {
                        case Language.It:
                            return "eterosessuale";

                        default:
                            return "straight";
                    }

                case "gay":
                    switch (SimpleI18n.Language)
                    {
                        case Language.It:
                            return (Gender == "F" ? "lesbica" : "gay");

                        default:
                            return (Gender == "F" ? "a lesbian" : "gay");
                    }
                    
                case "bi":
                    switch (SimpleI18n.Language)
                    {
                        case Language.It:
                            return "bisessuale";

                        default:
                            return "bisexual";
                    }
                case "trans":
                    switch (SimpleI18n.Language)
                    {
                        case Language.It:
                            return (Gender == "F" ? "una donna trans" : "un uomo trans");

                        default:
                            return (Gender == "F" ? "a trans woman" : "a trans man");
                    }

                case "poor":
                    switch (SimpleI18n.Language)
                    {
                        case Language.It:
                            return (Gender == "M" ? "povero" : "povera");

                        default:
                            return "poor";
                    }

                case "average":
                    switch (SimpleI18n.Language)
                    {
                        case Language.It:
                            return "di medio reddito";

                        default:
                            return "in the average-income bracket";
                    }

                case "wealthy":
                    switch (SimpleI18n.Language)
                    {
                        case Language.It:
                            return (Gender == "M" ? "ricco" : "ricca");

                        default:
                            return "wealthy";
                    }

                case "roma":
                    switch (SimpleI18n.Language)
                    {
                        case Language.It:
                            return "rom";

                        default:
                            return "Roma";
                    }

                case "white":
                    switch (SimpleI18n.Language)
                    {
                        case Language.It:
                            return Gender == "M" ? "bianco" : "bianca";

                        default:
                            return "White";
                    }

                case "black":
                    switch (SimpleI18n.Language)
                    {
                        case Language.It:
                            return Gender == "M" ? "nero" : "nera";

                        default:
                            return "Black";
                    }

                case "arab":
                    switch (SimpleI18n.Language)
                    {
                        case Language.It:
                            return Gender == "M" ? "arabo" : "araba";

                        default:
                            return "Arab";
                    }

                case "asian":
                    switch (SimpleI18n.Language)
                    {
                        case Language.It:
                            return Gender == "M" ? "asiatico" : "asiatica";

                        default:
                            return "Asian";
                    }

                case "jewish":
                    switch (SimpleI18n.Language)
                    {
                        case Language.It:
                            return Gender == "M" ? "ebreo" : "ebrea";

                        default:
                            return "Jewish"; // already means culturally so
                    }

                case "christian":
                    switch (SimpleI18n.Language)
                    {
                        case Language.It:
                            return "di cultura cristiana";

                        default:
                            return "culturally Christian";
                    }

                case "muslim":
                    switch (SimpleI18n.Language)
                    {
                        case Language.It:
                            return "di cultura musulmana";

                        default:
                            return "culturally Muslim";
                    }

                case "hindu":
                    switch (SimpleI18n.Language)
                    {
                        case Language.It:
                            return "di cultura indù";

                        default:
                            return "Hindu"; // already means culturally so
                    }

                case "traditional":
                    switch (SimpleI18n.Language)
                    {
                        case Language.It:
                            return "di una cultura tradizionale";

                        default:
                            return "part of a traditional culture";
                    }

                case "secular":
                    switch (SimpleI18n.Language)
                    {
                        case Language.It:
                            return Gender == "M" ? "laico" : "laica";

                        default:
                            return "secular";
                    }

                case "believer":
                    switch (SimpleI18n.Language)
                    {
                        case Language.It:
                            return "credente";

                        default:
                            return "a believer";
                    }

                case "migrant":
                    switch (SimpleI18n.Language)
                    {
                        case Language.It:
                            return Gender == "M" ? "un immigrato" : "un'immigrata";

                        default:
                            return "a migrant";
                    }

                case "illegal":
                case "illegal migrant":
                    switch (SimpleI18n.Language)
                    {
                        case Language.It:
                            return Gender == "M" ? "un immigrato senza documenti" : "un'immigrata senza documenti";

                        default:
                            return "an undocumented migrant";
                    }

                case "legal":
                case "legal migrant":
                    switch (SimpleI18n.Language)
                    {
                        case Language.It:
                            return Gender == "M" ? "un immigrato regolare" : "un'immigrata regolare";

                        default:
                            return "a legal migrant";
                    }

                case "2nd-gen":
                case "native 2nd-gen":
                    switch (SimpleI18n.Language)
                    {
                        case Language.It:
                            return (Gender == "M" ? "un autoctono" : "un'autoctona") + " (con genitori stranieri)";

                        default:
                            return "a native (with foreign parents)";
                    }

                case "native":
                    switch (SimpleI18n.Language)
                    {
                        case Language.It:
                            return Gender == "M" ? "un cittadino autoctono" : "una cittadina autoctona";

                        default:
                            return $"a native";
                    }

                default:
                        return Localized(tag);
            }
        }

        public string LocalizedGender => Localized(Gender);

        public string LocalizedRace => Localized(Race);

        public string LocalizedCreed => Localized(Creed);

        public string LocalizedWealth => Localized(Wealth);

        public string LocalizedSexuality => Localized(Sexuality);

        public string LocalizedCitizenship => Localized(Citizenship);


        private string Localized(string inEnglish)
        {
            switch (SimpleI18n.Language)
            {
                case Language.It:
                    switch (inEnglish.ToLowerInvariant())
                    {
                        case "m":
                        case "f":
                        case "n":
                            return inEnglish;

                        case "white":
                            return Gender == "M" ? "bianco" : "bianca";

                        case "black":
                            return Gender == "M" ? "nero" : "nera";

                        case "arab":
                            return Gender == "M" ? "arabo" : "araba";

                        case "roma":
                            return "rom";

                        case "jewish":
                            return Gender == "M" ? "ebreo" : "ebrea";

                        case "asian":
                            return Gender == "M" ? "asiatico" : "asiatica";

                        case "latino":
                            return Gender == "M" ? "latinoamericano" : "latinoamericana";

                        case "secular christian":
                            return Gender == "M" ? "cristiano laico" : "cristiana laica";

                        case "christian believer":
                            return Gender == "M" ? "cristiano praticante" : "cristiana praticante";

                        case "secular muslim":
                            return Gender == "M" ? "musulmano laico" : "musulmana laica";

                        case "muslim believer":
                            return Gender == "M" ? "musulmano praticante" : "musulmana praticante";

                        case "secular jewish":
                            return Gender == "M" ? "ebreo laico" : "ebrea laica";

                        case "jewish believer":
                            return Gender == "M" ? "ebreo praticante" : "ebrea praticante";

                        case "secular hindu":
                            return Gender == "M" ? "induista laico" : "induista laica";

                        case "hindu believer":
                            return Gender == "M" ? "induista praticante" : "induista praticante";

                        case "secular traditional":
                            return Gender == "M" ? "tradizionalista laico" : "tradizionalista laica";

                        case "traditional believer":
                            return "credente nella religione tradizionale";

                        case "average":
                            return "medio reddito";

                        case "poor":
                            return "povero";

                        case "wealthy":
                            return "ricco";

                        case "straight":
                            return "etero";

                        case "bi":
                            return "bisessuale";

                        case "gay":
                            return "omosessuale";

                        case "trans":
                            return "trans";

                        case "native":
                            return Gender == "M" ? "autoctono" : "autoctona";

                        case "legal migrant":
                            return Gender == "M" ? "immigrato regolare" : "immigrata regolare";

                        case "illegal migrant":
                            return Gender == "M" ? "immigrato irregolare" : "immigrata irregolare";

                        case "native 2nd-gen":
                            return "2ª generazione";

                        default:
                            return inEnglish;
                    }

                default:
                    return inEnglish;
            }
        }

        string It
        {
            get
            {
                switch (SimpleI18n.Language)
                {
                    case Language.It:
                        switch (Gender.ToLowerInvariant())
                        {
                            case "m":
                                return "Lui";

                            case "f":
                                return "Lei";

                            case "n":
                            default:
                                return "Questa persona";
                        }

                    default:
                        switch (Gender.ToLowerInvariant())
                        {
                            case "m":
                                return "He";

                            case "f":
                                return "She";

                            case "n":
                            default:
                                return "They";
                        }
                }
            }
        }

        private string ItIs
        {
            get
            {
                switch (SimpleI18n.Language)
                {
                    case Language.It:
                        switch (Gender.ToLowerInvariant())
                        {
                            case "m":
                            case "f":
                                return $"{It} è";

                            case "n":
                            default:
                                return "È";
                        }

                    default:
                        switch (Gender.ToLowerInvariant())
                        {
                            case "m":
                            case "f":
                                return $"{It} is";

                            case "n":
                            default:
                                return $"{It} are";
                        }
                }
                
            }
        }

        private string ItIsNot
        {
            get
            {
                switch (SimpleI18n.Language)
                {
                    case Language.It:
                        switch (Gender.ToLowerInvariant())
                        {
                            case "m":
                            case "f":
                                return $"{It} non è";

                            case "n":
                            default:
                                return "Non è";
                        }

                    default:
                        return $"{ItIs} not";
                }
            }
        }

        private string And
        {
            get
            {
                switch (SimpleI18n.Language)
                {
                    case Language.It:
                        return "e";
                    default:
                        return "and";
                }

            }
        }

        private string Its(string otherWordGender)
        {
            switch (SimpleI18n.Language)
            {
                case Language.It:
                    switch (otherWordGender.ToLower())
                    {
                        case "m":
                            return "Il suo";

                        case "f":
                        default:
                            return "La sua";
                    }

                default:
                    switch (Gender.ToLower())
                    {
                        case "m":
                            return "His";

                        case "f":
                            return "Her";

                        case "n":
                        default:
                            return "Their";
                    }
            }
        }

        private string ItsNeighbour(string otherPeoplesGender)
        {
            switch (SimpleI18n.Language)
            {
                case Language.It:
                    switch (otherPeoplesGender.ToLower())
                    {
                        case "m":
                            return $"{Its(otherPeoplesGender)} vicino";

                        case "f":
                        case "n":
                        default:
                            return $"{Its(otherPeoplesGender)} vicina";
                    }

                default:
                    return $"{Its(otherPeoplesGender)} neighbour";
            }
        }

        private readonly string[] wasDamaged = {
            "was scratched",
            "was harmed a bit",
            "went colder",
            "worsened",
            "went on a rough patch",
            "was negatively affected",
            "suffered some damage",
            "was quite damaged",
            "was severely disrupted",
            "was shattered"
        };

        private readonly string[] wasDamagedIt = {
            "è stato un po' intaccato",
            "è stato intaccato",
            "si è raffreddato",
            "è peggiorato",
            "è entrato un po' in crisi",
            "è stato compromesso",
            "è stato danneggiato",
            "si è rovinato",
            "si è interrotto",
            "è andato in frantumi"
        };

        private readonly string[] wereDamaged = {
            "were scratched",
            "were harmed a bit",
            "went colder",
            "worsened",
            "went on a rough patch",
            "were negatively affected",
            "suffered some damage",
            "were quite damaged",
            "were severely disrupted",
            "were shattered"
        };

        private readonly string[] wereDamagedIt = {
            "sono stati un po' intaccati",
            "sono stati intaccati",
            "si sono raffreddati",
            "sono peggiorati",
            "sono entrati un po' in crisi",
            "sono stati compromessi",            
            "sono stati danneggiati",
            "si sono rovinati",
            "si sono interrotti",
            "sono andati in frantumi"
        };

        private readonly string[] increased = {
            "grew a bit",
            "increased",
            "increased a lot",
            "made a big leap forward",
            "sky-rocketed",
        };

        private readonly string[] increasedIt = {
            "è cresciuta un po'",
            "è cresciuta",
            "è cresciuta molto",
            "ha fatto un balzo in avanti",
            "è schizzata alle stelle",
        };

        readonly string[] decreased = {
            "went down",
            "decreased",
            "decreased a lot",
            "dropped",
            "made a huge step back",
            "crumbled down",
            "collapsed",
            "sank to abysmal levels"
        };

        readonly string[] decreasedIt = {            
            "è diminuita",
            "è scesa",
            "è scesa parecchio",
            "è caduta",
            "ha fatto un grande passo indietro",
            "è crollata",
            "si è sgretolata",
            "è sprofondata"
        };

        readonly string[] palatable = {
            "palatable",
            "kind of interesting",
            "intriguing",
            "smart",
            "sort of fun",
            "very meaningful",
            "relatable",
            "right",
            "just right",
            "inspiring",
            "agreeable",
            "likeable",
            "very likeable",
            "simply the truth",
            "so true",
            "eye-opening"
        };

        readonly string[] palatableIt = {
            "accettabile",
            "tutto sommato interessante",
            "intrigante",
            "intelligente",
            "divertente, in un certo senso",
            "molto significativo",
            "toccante",
            "giusto",
            "proprio giusto",
            "di ispirazione",
            "molto condivisibile",
            "bello",
            "molto bello",
            "la pura e semplice verità",
            "verissimo",
            "rivelatore"
        };


        readonly string[] unpalatable = {
            "unpalatable",
            "in very poor taste",
            "unlikeable",
            "silly",
            "very silly",
            "stupid",
            "offensive",
            "outrageous",
            "repulsive",
            "annoying",
            "wrong",
            "plain wrong",
            "rubbish",
            "utter rubbish",
            "garbage",
            "utter garbage"
        };

        readonly string[] unpalatableIt = {
            "sgradevole",
            "di cattivo gusto",
            "spiacevole",
            "sciocco",
            "molto sciocco",
            "stupido",
            "offensivo",
            "oltraggioso",
            "repellente",
            "molesto",
            "sbagliato",
            "del tutto sbagliato",
            "spazzatura",
            "vera e propria spazzatura",
            "immondo",
            "assolutamente immondo"
        };

        readonly string[] disliked = {
            "did not like",
            "did not approve",
            "did not agree with",
            "disagreed with",
            "did not appreciate",
            "rejected",
            "ignored",
            "frowned upon"
        };

        readonly string[] dislikedIt = {
            "non l'ha gradito",
            "non l'ha approvato",
            "non è d'accordo",
            "è in disaccordo",
            "non l'ha apprezzato",
            "l'ha respinto",
            "l'ha ignorato",
            "ha espresso molta perplessità"
        };

        public int Hate(int strength, string[] negativeTargets)
        {
            int hate = 0;
            int largestRelationshipHarm = 0;
            NetworkMember mostAffectedRelationship = null;
            bool allRelationshipsEquallyAffected = true;
            foreach (Direction dir in Enum.GetValues(typeof(Direction)))
            {
                NetworkMember neighbour = GetNeighbour(dir);
                if (neighbour != null)
                {
                    List<string> whichTags;
                    if (neighbour.IsAnyOf(negativeTargets, out whichTags))
                    {
                        hate++;
                        whichTags.Shuffle();
                        string features = null;
                        int tagCount = 0;
                        foreach (string tag in whichTags)
                        {
                            string describedTag = neighbour.Describe(tag);
                            switch (tagCount++)
                            {
                                case 0:
                                    features = describedTag;
                                    break;

                                case 1:
                                    features = $"{describedTag} {And} {features}";
                                    break;

                                default:
                                    features = $"{describedTag}, {features}";
                                    break;
                            }
                        }
                        int effectiveStrength = strength + 5 * (whichTags.Count - 1);
                        if (effectiveStrength > largestRelationshipHarm)
                        {
                            if (mostAffectedRelationship != null)
                            {
                                allRelationshipsEquallyAffected = false;
                            }
                            largestRelationshipHarm = effectiveStrength;
                            mostAffectedRelationship = neighbour;
                        }

                        CurrentExperience
                            .Log(Language.It, $"{ItsNeighbour(neighbour.Gender)} {neighbour.Name} è {features}. ")
                            .Log(Language.En, $"{ItsNeighbour(neighbour.Gender)} {neighbour.Name} is {features}. ");

                        NeighbourRelationships[dir].Intensity -=
                            (effectiveStrength * DEIManager.I().Params.IntensityChangeAmplifier);
                        if (NeighbourRelationships[dir].Intensity < 0)
                        {
                            NeighbourRelationships[dir].Intensity = 0;
                        }
                    }
                }
            }

            if (largestRelationshipHarm == 0)
            {

                CurrentExperience
                    .Log(Language.It, $"Però, non ha avuto effetti col suo vicinato.")
                    .Log(Language.En, $"Still, this had no effect with {Its("n").ToLowerInvariant()} neighbours.");
            }
            else
            {
                if (hate == 1)
                {
                    CurrentExperience
                        .Log(Language.It, $"Il loro rapporto {PickFrom(wasDamagedIt, strength)}.")
                        .Log(Language.En, $"Their relationship {PickFrom(wasDamaged, strength)}.");
                }
                else
                {
                    CurrentExperience
                        .Log(Language.It, $"I rapporti con loro {PickFrom(wereDamagedIt, strength)}")
                        .Log(Language.En, $"Their relationships {PickFrom(wereDamaged, strength)}");
                    if (!allRelationshipsEquallyAffected)
                    {
                        CurrentExperience
                            .Log(Language.It, $", specialmente con {mostAffectedRelationship.Name}.", append: true)
                            .Log(Language.En, $", particularly the one with {mostAffectedRelationship.Name}.", append: true);
                    }
                    else
                    {
                        CurrentExperience.Log(".", append: true);
                    }
                }
            }

            ChangeEngagement(strength);
            return largestRelationshipHarm * hate;
        }

        public void Counterreact(int strength)
        {
            ChangeEngagement(-strength);
        }

        public void SetEngagement(int engagement)
        {
            this.engagement = engagement;
        }

        void ChangeEngagement(int strength)
        {
            if (strength > 0)
            {
                CurrentExperience
                    .Log(Language.It, $"{Its("f")} fiducia in te {PickFrom(increasedIt, strength)}. Ben fatto.")
                    .Log(Language.En, $"{Its("n")} trust in you {PickFrom(increased, strength)}. Well done.");
            }
            else
            {
                CurrentExperience
                    .Log(Language.It, $"{Its("f")} fiducia in te {PickFrom(decreasedIt, -strength)}.")
                    .Log(Language.En, $"{Its("n")} trust in you {PickFrom(decreased, -strength)}.");
            }
            engagement += strength;
            if (engagement < 0)
            {
                engagement = 0;
            }
            else if (engagement > 200)
            {
                engagement = 200;
            }
        }

        List<string> tags = null;
        readonly char[] separators = { ',', ' ' };

        bool IsAnyOf(string[] thoseTags, out List<string> whichTags)
        {
            whichTags = new List<string>();
            if (thoseTags.Length > 1 && thoseTags[0].ToLowerInvariant() == "none")
            {
                return false;
            }
            if (tags == null)
            {
                tags = new List<String>();
                foreach (string trait in new string[] { Gender, Race, Creed, Wealth, Sexuality, Citizenship })
                {
                    tags.AddRange(trait.Split(separators, StringSplitOptions.RemoveEmptyEntries));
                }
            }
            foreach (string thatTag in thoseTags)
            {
                string thatTagLower = thatTag.ToLowerInvariant();
                if (thatTagLower == "lgbt")
                {
                    if (Gender == "N")
                    {
                        whichTags.Add(Gender);
                    }
                    switch (Sexuality)
                    {
                        case "gay":
                        case "bi":
                        case "trans":
                            whichTags.Add(Sexuality);
                            break;

                        default:
                            break;
                    }
                }
                else if (thatTagLower == "poc")
                {
                    switch (Race)
                    {
                        case "White":
                        case "Jewish":
                        case "Roma":
                            break;

                        default:
                            whichTags.Add(Race);
                            break;
                    }
                }
                else if (thatTagLower == "non-christian")
                {
                    if (!Creed.ToLowerInvariant().StartsWith($"christian ") &&
                        !Creed.ToLowerInvariant().EndsWith($" christian"))
                    {
                        string[] creedParts = Creed.Split(separators);
                        foreach (string creedPart in creedParts)
                        {
                            switch (creedPart.ToLowerInvariant())
                            {
                                case "believer":
                                case "secular":
                                    break;

                                default:
                                    whichTags.Add(creedPart);
                                    break;
                            }
                        }
                    }
                }
                else foreach (string thisTag in tags)
                    {
                        string thisTagLower = thisTag.ToLowerInvariant();
                        if (thatTagLower == "all")
                        {
                            whichTags.Add(thisTag);
                        }
                        else if (thisTagLower == thatTagLower)
                        {
                            whichTags.Add(thatTag);
                        }
                    }
            }
            whichTags = whichTags.Distinct().ToList();
            return (whichTags.Count > 0);
        }

        public void Setup()
        {
            NeighbourRelationships = new Dictionary<Direction, NetworkRelationship>();
        }

        public NetworkMember GetNeighbour(Direction direction)
        {
            return GridPerson.GetNeighbour(direction)?.Myself;
        }

        public bool HasRelationshipWith(NetworkMember candidate)
        {
            foreach (NetworkRelationship relationship in NeighbourRelationships.Values)
            {
                if (relationship.With(candidate) > 0f)
                {
                    return true;
                }
            }
            return false;
        }

        public void BeginExperience(string contentText)
        {
            CurrentExperience = new Experience();
            CurrentExperience
                .Log(Language.It, $"{Name} ha letto il tuo post: «{contentText}».")
                .Log(Language.En, $"{Name} has read your post: «{contentText}».");
        }

        public void EndExperience()
        {
            if (CurrentExperience != null)
            {
                Experiences.Add(CurrentExperience);
                CurrentExperience = null;
            }
        }

        bool RandomEvent(int percentage)
        {
            return (UnityEngine.Random.Range(1, 100) <= percentage);
        }

        string PickAnyFrom(IList<string> strings)
        {
            return TT.OneRnd<string>(strings);
        }

        string PickFrom(IList<string> scale, int percentage)
        {
            int topElement = scale.Count - 1;
            percentage = percentage + UnityEngine.Random.Range(-15, +16);
            if (percentage > 100)
            {
                return scale[topElement];
            }
            if (percentage <= 0)
            {
                return scale[0];
            }
            return scale[percentage * topElement / 100];
        }

        string Description(IList<string> someTags)
        {
            return Describe(PickAnyFrom(someTags));
        }

        private static bool IsAgainstNone(string[] negativeTargets)
        {
            return (negativeTargets.Length == 0 || negativeTargets[0] == "none");
        }

        private static bool IsForAll(string[] positiveTargets)
        {
            return (positiveTargets.Length == 0 || positiveTargets[0] == "all");
        }

        private int AdjustedPalatability(int palatability)
        {

            if (palatability <= 0)
            {
                return 0;
            }

            // 100% engagement --> palatability unchanged
            // 200% engagement --> palatability up to 100%
            // 0% engagement --> palatability down to 0%
            // All other cases are proportional

            if (engagement >= 100)
            {
                float delta = (100 - palatability) * (engagement - 100);
                return palatability + Mathf.RoundToInt(delta);
            }
            else
            {
                int ap = Mathf.RoundToInt(palatability * engagement);
                if (ap < 2)
                {
                    return 2; // Prevent deadlocks
                }
                return ap;
            }
        }

        public bool FindsPlatable(string theme, int palatability, string[] positiveTargets, string[] negativeTargets)
        {
            List<string> whichTags;
            if (IsAnyOf(negativeTargets, out whichTags))
            {
                if (OvertonWindow.I.For(theme).Includes(palatability) &&
                    RandomEvent(Mathf.RoundToInt(AdjustedPalatability(palatability) * DEIManager.I().Params.OvertonOffensivePalatabilityFactor)))
                {
                    CurrentExperience
                        .Log(Language.It, $"\n{ItIs} {Description(whichTags)} ma questo tipo di contenuti ormai è considerato accettabile.")
                        .Log(Language.En, $"\n{ItIs} {Description(whichTags)} but this kind of content is well within the mainstream now.");
                    return true;
                }
                else if (IsAgainstNone(negativeTargets))
                {
                    CurrentExperience
                        .Log(Language.It, $"\n{It} ha trovato questo contenuto {PickAnyFrom(unpalatableIt)}.")
                        .Log(Language.En, $"\n{It} found the content {PickAnyFrom(unpalatable)}.");                    
                }
                else
                {
                    CurrentExperience
                        .Log(Language.It, $"\n{ItIs} {Description(whichTags)} e ha trovato questo contenuto {PickAnyFrom(unpalatableIt)}.")
                        .Log(Language.En, $"\n{ItIs} {Description(whichTags)} and found the content {PickAnyFrom(unpalatable)}.");
                    
                }
                return false;
            }
            else if (IsAnyOf(positiveTargets, out whichTags))
            {
                string oneFeature = Description(whichTags);
                if (RandomEvent(AdjustedPalatability(palatability)))
                {
                    if (IsForAll(positiveTargets))
                    {
                        CurrentExperience
                            .Log(Language.It, $"\n{It} ha trovato questo contenuto {PickAnyFrom(palatableIt)}.")
                            .Log(Language.En, $"\n{It} found the content {PickAnyFrom(palatable)}.");
                    }
                    else
                    {
                        CurrentExperience
                            .Log(Language.It, $"\n{ItIs} {oneFeature} e ha trovato questo contenuto {PickAnyFrom(palatableIt)}.")
                            .Log(Language.En, $"\n{ItIs} {oneFeature} and found the content {PickAnyFrom(palatable)}.");
                    }
                    return true;
                }
                else
                {
                    if (IsForAll(positiveTargets))
                    {
                        CurrentExperience
                            .Log(Language.It, $"\n{It} {PickAnyFrom(dislikedIt)}.")
                            .Log(Language.En, $"\n{It} {PickAnyFrom(disliked)} it.");
                    }
                    else
                    {
                        CurrentExperience
                            .Log(Language.It, $"\n{ItIs} {oneFeature} ma {PickAnyFrom(dislikedIt)}.")
                            .Log(Language.En, $"\n{ItIs} {oneFeature} but {PickAnyFrom(disliked)} it.");
                    }
                    return false;
                }
            }
            else
            {
                string oneFeature;
                if (!IsForAll(positiveTargets) && RandomEvent(50))
                {
                    oneFeature = Description(positiveTargets);
                }
                else
                {
                    switch (SimpleI18n.Language)
                    {
                        case Language.It:
                            switch (Gender)
                            {
                                case "M":
                                    oneFeature = "un fanatico";
                                    break;

                                case "F":
                                    oneFeature = "una fanatica";
                                    break;

                                default:
                                    oneFeature = "una persona fanatica";
                                    break;
                            }
                            break;

                        default:
                            oneFeature = "a fanatic";
                            break;
                    }
                    
                }
                if (OvertonWindow.I.For(theme).Includes(palatability) && RandomEvent(AdjustedPalatability(palatability)))
                {
                    CurrentExperience
                        .Log(Language.It, $"\n{ItIsNot} {oneFeature} ma questo tipo di contenuti ormai è considerato accettabile.")
                        .Log(Language.En, $"\n{ItIsNot} {oneFeature} but this kind of content is well within the mainstream now.");
                    return true;
                }
                else
                {
                    CurrentExperience
                        .Log(Language.It, $"\n{ItIsNot} {oneFeature} e {PickAnyFrom(dislikedIt)}.")
                        .Log(Language.En, $"\n{ItIsNot} {oneFeature} and {PickAnyFrom(disliked)} it.");
                    return false;
                }
            }
        }



        public void RestoreConfidence()
        {
            if (engagement < 100)
            {
                engagement += (100 / DEIManager.I().Params.TurnsToRestoreConfidence);
                if (engagement > 100)
                {
                    engagement = 100;
                }
            }
        }
    }
}
