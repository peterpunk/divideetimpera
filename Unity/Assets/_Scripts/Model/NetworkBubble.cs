﻿using DG.Debugging;
using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace DivideEtImpera
{
    public class NetworkBubble
    {
        List<NetworkMember> Members = new List<NetworkMember>();

        public bool Empty {
            get { return Members.Count == 0; }
        }

        public int Size
        {
            get { return Members.Count; }
        }

        public NetworkBubble(NetworkMember founder)
        {
            // Delogger.Log(This(), $"Start from {founder.Name}");
            Members.Add(founder);
        }

        public void Engulf(NetworkBubble bubble)
        {
            // Delogger.Log(This(), $"Engulf {bubble.This()}");
            foreach (NetworkMember member in bubble.Members)
            {
                Include(member);
            }            
            bubble.Members.Clear();
        }

        public void Include(NetworkMember member)
        {
            // Delogger.Log(This(), $"Include {member.Name}?");
            if (!Includes(member))
            {
                Members.Add(member);
            }
        }

        public bool Includes(NetworkMember member)
        {
            return Members.Contains(member);
        }

        public bool MustInclude(NetworkMember candidate)
        {
            // Delogger.Log(This(), $"MustInclude {candidate.Name}");
            if (Includes(candidate))
            {
                return true;
            }

            foreach (NetworkMember member in Members)
            {
                if (member.HasRelationshipWith(candidate))
                {
                    return true;
                }
            }
            return false;
        }

        public void PaintAsLargest()
        {
            foreach (NetworkMember member in Members)
            {
                member.GridPerson.PaintPortraitBackground();
            }
        }

        public void PaintAsSmaller()
        {
            foreach (NetworkMember member in Members)
            {
                member.GridPerson.PaintPortraitBackground(Color.gray);
            }
        }

        private string This()
        {
            if (Empty)
            {
                return $"NetworkBubble (empty)";
            }
            else
            {
                return $"NetworkBubble ({Members[0].Name} + {Members.Count - 1} more)";
            }            
        }
    }
}