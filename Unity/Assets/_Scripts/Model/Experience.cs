﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace DivideEtImpera
{
    public class Experience
    {
        List<string> Events = new List<string>();

        internal Experience Log(string v, bool append = false)
        {
            if (!append || Events.Count == 0)
            {
                Events.Add(v);
            }
            else 
            {
                Events[Events.Count - 1] += v;
            }                        
            //Debug.Log(v);
            return this;
        }

        internal Experience Log(Language lang, string v, bool append = false)
        {
            if (SimpleI18n.Language == lang)
            {
                Log(v, append);
            }
            return this;
        }

        public string Print()
        {
            StringBuilder  sb = new StringBuilder();
            foreach (var @event in Events)
            {
                sb.Append(@event).Append(" ");
            }

            return sb.ToString();
        }


    }
}