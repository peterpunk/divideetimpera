﻿using DG.Debugging;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace DivideEtImpera
{
    public class OvertonWindow : MonoBehaviour
    {
        public static OvertonWindow I;
        private Dictionary<string, OvertonSubwindow> subwindows = new Dictionary<string, OvertonSubwindow>();

        void Awake()
        {
            I = this;
        }

        public OvertonSubwindow For(string theme)
        {
            if (!subwindows.ContainsKey(theme))
            {
                subwindows[theme] = new OvertonSubwindow(theme);
            }
            return subwindows[theme];
        }

        public class OvertonSubwindow
        {
            private string theme;

            private readonly float STRETCH = DEIManager.I().Params.OvertonPatalabilityMainThemeStretch;
            private readonly float SLIGHT_STRETCH = DEIManager.I().Params.OvertonPatalabilitySubthemeStretch;            

            private float boundary = 100;

            public OvertonSubwindow(string theme)
            {
                this.theme = theme;
            }

            public bool Includes(int palatability)
            {
                return palatability >= boundary;
            }

            public bool Excludes(int palatability)
            {
                return !Includes(palatability);
            }

            public void StretchTowards(int palatability)
            {
                boundary -= STRETCH;
            }

            public void SlightlyStretchTowards(int palatability)
            {
                boundary -= SLIGHT_STRETCH;
            }
            

            public void Log()
            {
                Delogger.Log($"OvertonSubwindow for {theme}", $"boundary = {boundary}");
            }
        }
    }
}