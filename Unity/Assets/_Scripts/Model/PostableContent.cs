﻿using DG.Debugging;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DivideEtImpera
{
    public class PostableContent
    {     

        public string Text;

        public Dictionary<Language, List<string>> Variants = new Dictionary<Language, List<string>>();
        public string Theme;
        public string Subtheme;
        public int Palatability;
        public string PalatableTo;
        public int Strength;
        public string OffensiveTo;

        readonly char[] separators = {',', ' '};

        public void InitialiseVariants()
        {
            foreach (Language language in SimpleI18n.AllLanguages)
            {
                if (language != Language.Undecided)
                {
                    Variants.Add(language, new List<string>());
                }                
            }            
        }

        public string[] PositiveTargets
        {
            get
            {
                return PalatableTo.Split(separators, StringSplitOptions.RemoveEmptyEntries);
            }
        }

        public string[] NegativeTargets
        {
            get
            {
                return OffensiveTo.Split(separators, StringSplitOptions.RemoveEmptyEntries);
            }
        }

        internal int Affect(NetworkMember citizen)
        {
            int outcome;
            citizen.BeginExperience(Text);
            if (citizen.FindsPlatable(Theme, Palatability, PositiveTargets, NegativeTargets))
            {
                outcome = citizen.Hate(Strength, NegativeTargets);

                if (OvertonWindow.I.For(Theme).Excludes(Palatability))
                {
                    OvertonWindow.I.For(Theme).StretchTowards(Palatability);
                }
                else if (OvertonWindow.I.For(Subtheme).Excludes(Palatability))
                {
                    OvertonWindow.I.For(Subtheme).SlightlyStretchTowards(Palatability);
                }
            }
            else
            {
                outcome = -1;
                citizen.Counterreact(Strength);
            }
            citizen.EndExperience();
            return outcome;
        }

        public void Log()
        {
            Delogger.Log("PostableContent", $"Palatability={Palatability}, Strength={Strength}");
        }

        public List<String> LocalisedVariants
        {
            get { return Variants[SimpleI18n.Language]; }
        }

            
    }
}
