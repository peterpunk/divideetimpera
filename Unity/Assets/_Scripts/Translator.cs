﻿using OL;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Translator : MonoBehaviour
{    
    private static Dictionary<Language, Dictionary<string, string>> Dictionaries = new Dictionary<Language, Dictionary<string, string>>();
    private static Setuppable LocalizedGUISetup = Setuppable.NewInstance();

    public bool Production;

    private Language CurrentLanguage = Language.Undecided;

    // Start is called before the first frame update
    void Start()
    {
        foreach (Language language in SimpleI18n.AllLanguages)
        {
            if (!Dictionaries.ContainsKey(language))
            {
                Dictionaries.Add(language, new Dictionary<string, string>());
            }
        }
        if (!Production)
        {
            Debug.Log("Development release - switch on the Production flag in the Translator component before releasing");
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (LocalizedGUISetup.IsToBeSetupped())
        {
            LocalizedGUISetup.Setupping();

            if (!Production)
            {
                StartCoroutine(TT.DownloadCSVCoroutine("1pxFcsSDObwEXJeWGkpBOC6LqNaGF7Urm2OjAI5UQyWM",
                    (csv) =>
                    {
                        ImportContent(csv);
                        LocalizedGUISetup.SetupCompleted();
                    }, true, "Localized GUI", "1995799682"));
            }
            else
            {
                var data = Resources.Load("Localized GUI") as TextAsset;
                ImportContent(data.text);
                LocalizedGUISetup.SetupCompleted();
            }
        }
        
        if (LocalizedGUISetup.IsSetupped())
        {
            if (CurrentLanguage != SimpleI18n.Language)
            {
                TranslateLabels(SimpleI18n.Language);                
                CurrentLanguage = SimpleI18n.Language;
            }
        }
        
    }

    public void TranslateLabels(Language language)
    {
        foreach (TMP_Text tmp in GetComponentsInChildren<TMP_Text>(true))
        {
            tmp.SetText(Translate(tmp.text, language));
        }
    }

    public void ImportContent(string csv)
    {
        List<List<string>> rawData = TT.ParseCSV(csv);

        foreach (var alist in rawData)
        {
            int i = 0;
            var key = alist[i].Trim();
            var en = alist[++i].Trim();
            var it = alist[++i].Trim();

            if (TT.IsNullOrWhitespace(key) || TT.IsNullOrWhitespace(en))
                continue;

            if (TT.IsNullOrWhitespace(it))
            {
                it = en;
            }

            Debug.Log("Storing localized strings for $" + key);
            Dictionaries[Language.En][key] = en;
            Dictionaries[Language.It][key] = it;
        }
    }

    public static string _(string sentence)
    {
        return Translate(sentence, SimpleI18n.Language);
    }

    private static string Translate(string sentence, Language language)
    {
        string[] words = sentence.Split();
        foreach (string word in words)
        {
            if (word.StartsWith("$"))
            {
                Debug.Log($"Localizing string {word} into {language}");
                string key = word.Substring(1);
                try
                {
                    sentence = sentence.Replace(word, Dictionaries[language][key]);
                }
                catch (KeyNotFoundException knfe)
                {
                    Debug.LogWarning($"Skipping {word}: {knfe.Message}");
                }
            }
        }
        return sentence;
    }
}
