﻿using System.Collections;
using System.Collections.Generic;
using OL;
using UnityEngine;

namespace DivideEtImpera
{
    public class DEITimeManager : SimpleGameTimeManager
    {
        void Awake()
            {
                if (InjectedReferrableInstance == null)
                {
                    InjectedReferrableInstance = this;
                    //Setup();
                    DontDestroyOnLoad(gameObject);
                }
                else
                {
                    Destroy(gameObject);
                }
            }

            public static DEITimeManager I()
            {
                return (DEITimeManager)InjectedReferrableInstance;
            }


        public override void OnUpdate()
        {

        }

        public override float FractionOfRealTime()
        {
            /*if (MatchManager.I != null)
            {
                if (MatchManager.I.State == MatchManager.MatchState.RUNNING_SLO_MO)
                    return .1f;
                else
                    return 1f;
            }*/
            return 1f;
        }
    }
}
