﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using DG.Tweening;
using OL;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Random = UnityEngine.Random;
using Direction = DivideEtImpera.Person.Direction;
using DG.Debugging;

namespace DivideEtImpera
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public class PostManager : MonoBehaviour
    {
        [SerializeField] Transform IntroLabel;
        [SerializeField] Transform PostTitleLabel;

        [SerializeField] Transform PostedContentRoot;
        [SerializeField] Transform NewContentRoot;
        [SerializeField] Transform TargetingContent;

        [SerializeField] Button AheadButton;

        [SerializeField] LayoutElement PostedContentLayout;
        [SerializeField] LayoutElement NewContentLayout;
        [SerializeField] LayoutElement TargetingLayout;

        [SerializeField] LayoutElement TargetingIntro;    
        [SerializeField] Button TargetingConfirmButton;

        [SerializeField] Transform ContentPrefab;
        [SerializeField] Transform PostedContentPrefab;
        [SerializeField] Transform TipPrefab;
        [SerializeField] Transform GoalUpdatePrefab;

        [SerializeField]
        CanvasGroup Detail;

        [SerializeField]
        CanvasGroup End;
        [SerializeField]
        TextMeshProUGUI EndTitleText;
        [SerializeField]
        TextMeshProUGUI EndText;
        [SerializeField]
        Button RestartButton;

        [SerializeField]
        Image Focused;
        [SerializeField]
        Transform North;
        [SerializeField]
        Transform South;
        [SerializeField]
        Transform East;
        [SerializeField]
        Transform West;

        // SESSION
        public PlaySession Session;

        
        public static event Action<PostingPhase> PostingPhaseChanged = delegate { };

        public enum PostingPhase
        {
            NONE,
            PICKING_TEXT,
            PICKING_AREA,
            PICKED_AREA_PREVIEW,
            PROPAGATE
        }

        public static PostManager I;
        private bool NeedsNewContent;
        private float nextTipTime;
        private int tipWaitingFactor;
        private int tipIndex;
        private List<string> tips = new List<string>();
        private List<string> Tips
        {
            get
            {
                if (tips.Count == 0)
                {
                    Dictionary<Language, List<string>> allTips = DEIManager.I().AllTips;
                    if (allTips != null && allTips.Count > 0)
                    {
                        tips = DEIManager.I().AllTips[SimpleI18n.Language];
                    }
                }
                return tips;
            }
        }

        void Awake()
        {
            I = this;
        }


        void Start()
        {
            Session = new PlaySession();
            Session.Phase = PostingPhase.NONE;
            End.alpha = 0;
            End.gameObject.SetActive(false);
            NeedsNewContent = true;
            tipIndex = 0;
            PauseTips();
        }
        

        void Update()
        {
            if (tipIndex < Tips.Count && Time.time > nextTipTime)
            {
                float tipChance = Mathf.Max(50, 100 - (tipIndex * 5));
                if (Random.Range(0, 99) < tipChance)
                {
                    GiveTip(Tips[tipIndex]);
                    tipIndex++;
                }
                ReadyForTips();
            }
        }

        public void GoAheadToNewContent()
        {
            GoToNewContent();
            if (NeedsNewContent)
            {
                PrepareNewContent();
                NeedsNewContent = false;
            }
        }

        public void GoBackToNewContent()
        {
            ExitPreview();
            GoToNewContent();
        }

        void GoToNewContent()
        {
            PostedContentLayout.gameObject.SetActive(false);
            NewContentLayout.gameObject.SetActive(true);
            TargetingLayout.gameObject.SetActive(false);
            ChangePhase(PostingPhase.NONE);
        }

        void PrepareNewContent()
        {
            PauseTips();
            if (Session.Phase != PostingPhase.NONE)
            {
                return;
            }                

            foreach (Button toRemove in NewContentRoot.GetComponentsInChildren<Button>()) {
                Destroy(toRemove.gameObject);
            }
            for (int i = 0; i < 3; i++)
            {
                Transform newContentTransform = Instantiate(ContentPrefab, NewContentRoot);
                PostableContent newContentData = DEIManager.I().RandomFreshContent();
                newContentTransform.GetComponentInChildren<TextMeshProUGUI>().text = newContentData.Text;
                newContentTransform.GetComponent<Button>().onClick.AddListener(delegate {
                    GoToTargeting(newContentData);
                });
            }

            ChangePhase(PostingPhase.PICKING_TEXT);
        }

        void ChangePhase(PostingPhase newPhase)
        {
            Session.Phase = newPhase;
            PostingPhaseChanged(Session.Phase);
        }

        void GoToTargeting(PostableContent newContentData)
        {
            Session.PickedContentData = newContentData;
            PostedContentLayout.gameObject.SetActive(false);
            NewContentLayout.gameObject.SetActive(false);
            TargetingLayout.gameObject.SetActive(true);
            TargetingIntro.gameObject.SetActive(true);
            TargetingConfirmButton.gameObject.SetActive(false);
            TargetingContent.GetComponentInChildren<TextMeshProUGUI>().text = newContentData.Text;

            ChangePhase(PostingPhase.PICKING_AREA);
        }

        public void GoToPostedContent()
        {
            CheckAheadButton();
            PostedContentLayout.gameObject.SetActive(true);
            NewContentLayout.gameObject.SetActive(false);
            TargetingLayout.gameObject.SetActive(false);
        }

        public void EnterPreview(Person person)
        {
            TargetingIntro.gameObject.SetActive(false);
            TargetingConfirmButton.gameObject.SetActive(true);
            Session.PickedPerson = person;
            ChangePhase(PostingPhase.PICKED_AREA_PREVIEW);
        }

        public void Post()
        {
            Session.PickedContentData.Log();

            foreach (Person person in GridManager.I.AllGridElements)
            {
                // Confidence is slowly restored for negatively impressed people
                person.Myself.RestoreConfidence();
            }

            Dictionary<Person, int> outcomes = new Dictionary<Person, int>();
            foreach (Person affectedPerson in Session.PickedPerson.SelectionGroup)
            {                
                int hate = affectedPerson.Absorb(Session.PickedContentData);
                outcomes.Add(affectedPerson, hate);
            }
            OvertonWindow.I.For(Session.PickedContentData.Theme).Log();
            OvertonWindow.I.For(Session.PickedContentData.Subtheme).Log();
            Person best = null;
            Person worst = null;
            foreach (KeyValuePair<Person, int> outcome in outcomes)
            {
                Person affected = outcome.Key;
                int hate = outcome.Value;
                //Debug.Log(outcome.Key.Myself.Name + " got " + hate);

                if (hate >= 0)
                {
                    affected.Flavor = (hate == 0) ? Person.FlavorType.NEUTER : Person.FlavorType.POSITIVE;
                    if ((best == null || hate > outcomes[best]))
                    {
                        best = affected;
                    }
                }
                else
                {
                    affected.Flavor = Person.FlavorType.NEGATIVE;
                    if (worst == null || hate < outcomes[worst])
                    {
                        worst = affected;
                    }
                }
            }
            if (best != null)
            {
                //Debug.Log(best.Myself.Name + " got the best outcome");
                best.Flavor = Person.FlavorType.MOST_POSITIVE;
            }
            if (worst != null)
            {
                //Debug.Log(worst.Myself.Name + " got the worst outcome");
                worst.Flavor = Person.FlavorType.MOST_NEGATIVE;
            }

            ChangePhase(PostingPhase.PROPAGATE);
            ExitPreview();
            NeedsNewContent = true;            

            Session.CurrentContentIsConfirmed();

            IntroLabel.gameObject.SetActive(false);
            PostTitleLabel.gameObject.SetActive(true);

            Transform newContentTransform = Instantiate(PostedContentPrefab, PostedContentRoot);
            newContentTransform.SetAsFirstSibling();
            newContentTransform.GetComponentInChildren<Post>().SetText(Session.PickedContentData.Text);
            int previousLargestBubbleSize = Session.LargestBubbleSize;
            Session.LargestBubbleSize = GridManager.I.UpdateGrid();

            if (previousLargestBubbleSize != Session.LargestBubbleSize)
            {
                GiveGoalUpdate();
            }

            ReadyForTips();
            GoToPostedContent();
        }

        private void ReadyForTips()
        {            
            nextTipTime = Time.time + tipWaitingFactor * Random.Range(3, 8);
            tipWaitingFactor *= 3;
        }

        private void PauseTips()
        {
            nextTipTime = float.PositiveInfinity;
            tipWaitingFactor = 1;
        }

        internal void GiveTip(string text)
        {
            Transform newTipTransform = Instantiate(TipPrefab, PostedContentRoot);
            newTipTransform.SetAsFirstSibling();
            newTipTransform.GetComponentInChildren<Tip>().SetText(text);
        }

        internal void GiveGoalUpdate()
        {            
            Transform newGoalUpdateTransform = Instantiate(GoalUpdatePrefab, PostedContentRoot);
            newGoalUpdateTransform.SetAsFirstSibling();
            newGoalUpdateTransform.GetComponentInChildren<GoalUpdate>().SetLargestBubble(Session.LargestBubbleSize);
        }

        internal void CheckAheadButton()
        {
            foreach (Person person in GridManager.I.AllGridElements)
            {
                if (person.Flavor == Person.FlavorType.MOST_POSITIVE || person.Flavor == Person.FlavorType.MOST_NEGATIVE)
                {
                    SetAheadButton(false);
                    return;
                }
            }
            SetAheadButton(true);
        }

        private void SetAheadButton(bool on)
        {
            bool gameOver = HasWon();

            AheadButton.interactable = on;
            AheadButton.GetComponentInChildren<TextMeshProUGUI>().SetText(Translator._(on ? "$PREPARE_NEXT" : 
                    (gameOver ? "$BEHOLD" : "$BEHOLD_COMMA\n$THEN_POST")));

            if (on && gameOver)
            {
                DoEnd();
            }

        }

        public void ExitPreview()
        {
            if (Session.PickedPerson != null)
            {
                Session.PickedPerson.DeactivatePreview();
            }            
        }
        
        public void DoCancel()
        {
            ChangePhase(PostingPhase.NONE);
        }

        public bool HasWon()
        {
            return (Session.LargestBubbleSize <= DEIManager.I().Params.LargestBubbleSizeToWin);
        }

        void DoEnd()
        {
            int posts = Session.AllPostedContents.Count;
            int n = DEIManager.I().Params.MinMovesForBadOutcome;
            if (posts < n / 3)
            {
                EndText.text = Translator._("$BEST_END");
            }
            else if (posts < n / 2)
            {
                EndText.text = Translator._("$GOOD_END");
            }
            else if (posts < n)
            {
                EndText.text = Translator._("$OK_END");
            }
            else
            {
                EndText.text = Translator._("$BAD_END");
            }
            EndText.text = EndText.text.Replace("((POSTS))", posts.ToString());
            End.gameObject.SetActive(true);
            AheadButton.gameObject.SetActive(false);
            End.DOFade(1, 1);
        }

        public void Restart()
        {
            RestartButton.enabled = false;
            DOTween.KillAll();
            Destroy(DEIManager.I().gameObject);
            SceneManager.LoadScene("SocialNetwork");
        }
        public void LearnMore(string anUrl)
        {
            Application.OpenURL(anUrl);
        }
    }
}
