﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Debugging;
using DG.DeExtensions;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace DivideEtImpera
{
    public class Person : MonoBehaviour
    {
        public enum Direction
        {
            NORTH,
            SOUTH,
            EAST,
            WEST
        }

        public int Row;
        public int Column;

        public NetworkMember Myself;

        public Dictionary<Direction, Person> Neighbours;

        public List<Image> PortraitBackgrounds;
        public Image Portrait;

        public Image SelectionArea;

        public Image BigThumbUp;
        public Image BigThumbDown;
        public Image SmallThumbUp;
        public Image SmallThumbDown;

        private List<Tween> IntenseTweens = new List<Tween>();

        public List<Image> Hat;
        public Image Badge;

        [TextArea(3, 6)]
        public string DebugContent;

        float lastUpdateDebug;

        public enum FlavorType
        {
            MOST_POSITIVE,
            POSITIVE,
            NEUTER,
            NEGATIVE,
            MOST_NEGATIVE
        }
        public bool IntenseFlavor;

        public FlavorType Flavor
        {
            get
            {
                if (BigThumbUp.enabled)
                {
                    return FlavorType.MOST_POSITIVE;
                }
                if (SmallThumbUp.enabled)
                {
                    return FlavorType.POSITIVE;
                }
                if (BigThumbDown.enabled)
                {                   
                    return FlavorType.MOST_NEGATIVE;
                }
                if (SmallThumbDown.enabled)
                {
                    return FlavorType.NEGATIVE;
                }
                return FlavorType.NEUTER;
            }

            set
            {
                BigThumbUp.enabled = false;
                SmallThumbUp.enabled = false;
                BigThumbDown.enabled = false;
                SmallThumbDown.enabled = false;

                switch (value)
                {
                    case FlavorType.MOST_POSITIVE:
                        BigThumbUp.enabled = true;
                        break;

                    case FlavorType.POSITIVE:
                        SmallThumbUp.enabled = true;
                        FadeOut(SmallThumbUp);
                        break;

                    case FlavorType.NEGATIVE:
                        SmallThumbDown.enabled = true;
                        FadeOut(SmallThumbDown);
                        break;

                    case FlavorType.MOST_NEGATIVE:
                        BigThumbDown.enabled = true;
                        break;

                    case FlavorType.NEUTER:
                        // All disabled
                        break;
                }
            }
        }

        private void FadeOut(Image thumb)
        {
            thumb.SetAlpha(1f);
            thumb.DOFade(0f, 1f).SetDelay(5).OnComplete(() => { if (thumb.enabled) Flavor = FlavorType.NEUTER; });
        }

        void Awake()
        {
            DisplayHat(false);
            DisplayBadge(false);            
        }

        void Start()
        {
            Neighbours = new Dictionary<Direction, Person>();
            GridManager.I.AllGridElements.Add(this);
            Flavor = FlavorType.NEUTER;
            foreach (Transform t in new Transform[] { BigThumbUp.rectTransform, BigThumbDown.rectTransform })
            {
                t.DOScale(1.3f, 0.5f).SetLoops(-1, LoopType.Yoyo);
            }
        }

        void Update()
        {
            if (!DEIManager.I().Production && Time.time - lastUpdateDebug > 2 && !DEIManager.NotReadyYet())
            {
                lastUpdateDebug = Time.time;
                DebugContent = $"Myself {Myself.Name}\n" +
                               $"Neighbours {Neighbours.Count}";
            }
        }

        public void Setup(NetworkMember member)
        {
            Myself = member;
            Myself.SetEngagement(Random.Range(
                DEIManager.I().Params.InitialEngagementMin,
                DEIManager.I().Params.InitialEngagementMax));
            member.GridPerson = this;

            //var picked = Random.Range(0, DEIManager.I().Assets.PersonTypes.Count);
            //todo be coherent with myself
            Portrait.sprite = DEIManager.I().Assets.GetSpriteByName(Myself.Name);
            PostManager.PostingPhaseChanged += OnPostingPhaseChanged;
            
            DisplayHat(false);
            Badge.gameObject.SetActive(false);
        }

        public List<Person> SelectionGroup
        {
            get
            {
                List<Person> group = new List<Person>();

                // Find the center of this person's selection area
                Person center = this;
                if (!center.Neighbours.ContainsKey(Direction.NORTH)) // Northern border
                {
                    center = center.Neighbours[Direction.SOUTH];
                }
                else if (!center.Neighbours.ContainsKey(Direction.SOUTH)) // Southern border
                {
                    center = center.Neighbours[Direction.NORTH];
                }
                if (!center.Neighbours.ContainsKey(Direction.EAST)) // Eastern border
                {
                    center = center.Neighbours[Direction.WEST];
                }
                else if (!center.Neighbours.ContainsKey(Direction.WEST)) // Western border
                {
                    center = center.Neighbours[Direction.EAST];
                }

                // Add the center and the surrounding 8 persons to the group
                group.Add(center);
                group.Add(center.Neighbours[Direction.NORTH].Neighbours[Direction.WEST]);
                group.Add(center.Neighbours[Direction.NORTH]);
                group.Add(center.Neighbours[Direction.NORTH].Neighbours[Direction.EAST]);
                group.Add(center.Neighbours[Direction.EAST]);
                group.Add(center.Neighbours[Direction.SOUTH].Neighbours[Direction.EAST]);
                group.Add(center.Neighbours[Direction.SOUTH]);
                group.Add(center.Neighbours[Direction.SOUTH].Neighbours[Direction.WEST]);
                group.Add(center.Neighbours[Direction.WEST]);

                return group;
            }
        }

        void KillPickingAreaEffects()
        {
            DOTween.Kill(PostManager.PostingPhase.PICKING_AREA + Myself.Name, true);
            Portrait.rectTransform.DOScale(new Vector3(1, 1, 0), 0.5f);
        }

        private void KillPickedAreaPreviewEffects()
        {
            DOTween.Kill(PostManager.PostingPhase.PICKED_AREA_PREVIEW + Myself.Name, true);
        }

        void OnPostingPhaseChanged(PostManager.PostingPhase newPhase)
        {
            switch (newPhase)
            {
                case PostManager.PostingPhase.NONE:
                    KillPickingAreaEffects();
                    KillPickedAreaPreviewEffects();
                    Portrait.color = Color.white;
                    break;

                case PostManager.PostingPhase.PICKING_TEXT:
                    KillPickingAreaEffects();
                    break;

                case PostManager.PostingPhase.PICKING_AREA:
                    Portrait.rectTransform.DOScale(new Vector3(1.2f, 1.2f, 0), 0.7f).SetLoops(-1, LoopType.Yoyo).
                        SetId(PostManager.PostingPhase.PICKING_AREA + Myself.Name);
                    break;

                case PostManager.PostingPhase.PICKED_AREA_PREVIEW:
                    KillPickingAreaEffects();
                    break;

                case PostManager.PostingPhase.PROPAGATE:
                    KillPickedAreaPreviewEffects();
                    List<Person> selectionGroup = PostManager.I.Session.PickedPerson.SelectionGroup;
                    if (selectionGroup.Contains(this))
                    {
                        Portrait.DOColor(Color.red, 1f).SetLoops(2, LoopType.Yoyo);
                    }
                    else if (this.IsNeighbourOf(selectionGroup))
                    {
                        Portrait.DOColor(Color.yellow, 1f).SetDelay(0.2f).SetLoops(2, LoopType.Yoyo);
                    }
                    else
                    {
                        Portrait.DOColor(Color.white, 0.5f);
                    }

                    UpdateHatAndBadge();
                    
                    break;
            }


        }

        public void UpdateHatAndBadge()
        {
            Tuple<bool, bool> hatAndBadge = HatAndBadge();
            DisplayHat(hatAndBadge.Item1);
            DisplayBadge(hatAndBadge.Item2);
        }

        public Tuple<bool, bool> HatAndBadge()
        {
            var engagementVariation = Myself.EngagementVariation();
            // Delogger.Log("OnPostingPhaseChanged", $"{Myself.Name} EngagementVariation {engagementVariation}");

            if (engagementVariation > (DEIManager.I().Params.MinPercToActivateBadge / 100))
            {
                return new Tuple<bool, bool>(true, true);
            }
            if (engagementVariation > (DEIManager.I().Params.MinPercToActivateHat / 100))
            {
                return new Tuple<bool, bool>(true, false);
            }
            return new Tuple<bool, bool>(false, false);
        }

        internal int Absorb(PostableContent pickedContentData)
        {
            return pickedContentData.Affect(Myself);
            //TODO Visual reaction
        }

        public bool IsNeighbourOf(Person person)
        {
            foreach (var pn in person.Neighbours.Values)
            {
                //Delogger.Log("Comparing",$"{Myself.Name} {pn.Myself.Name}");
                if (Myself.Name == pn.Myself.Name)
                    return true;
            }

            return false;
        }

        private bool IsNeighbourOf(List<Person> group)
        {
            foreach (Person person in group)
            {
                if (IsNeighbourOf(person))
                {
                    return true;
                }
            }
            return false;
        }

        public void OnClick()
        {
            if (PostManager.I.Session.Phase == PostManager.PostingPhase.PICKING_AREA)
            {
                ActivatePreview();
            }
            else if (PostManager.I.Session.Phase == PostManager.PostingPhase.PICKED_AREA_PREVIEW)
            {
                PostManager.I.ExitPreview();
                ActivatePreview();
            }
            else
            {
                PersonDrawer.I.OpenDetail(this);
            }
        }

        void ActivatePreview()
        {
            PostManager.I.EnterPreview(this);

            SelectionArea.SetAlpha(0);
            SelectionArea.gameObject.SetActive(true);
            SelectionArea.DOFade(1, 1).SetLoops(-1, LoopType.Yoyo)
                .SetId(PostManager.PostingPhase.PICKED_AREA_PREVIEW + Myself.Name);
        }

        public void DeactivatePreview()
        {
            SelectionArea.SetAlpha(0);
            SelectionArea.gameObject.SetActive(false);
            KillPickedAreaPreviewEffects();
        }

        public Person GetNeighbour(Direction direction)
        {
            if (Neighbours.ContainsKey(direction))
                return Neighbours[direction];
            return null;
        }

        public static Person GetPersonAtGrid(int row, int column)
        {
            foreach (var person in GridManager.I.AllGridElements)
            {
                if (person.Row == row && person.Column == column)
                    return person;
            }

            return null;
        }

        void OnDestroy()
        {
            PostManager.PostingPhaseChanged -= OnPostingPhaseChanged;
        }

        public void PaintPortraitBackground()
        {
            PaintPortraitBackground(Color.white);
        }

        public void PaintPortraitBackground(Color color)
        {
            foreach (Image portraitBackground in PortraitBackgrounds)
            {
                portraitBackground.color = color;
            }            
        }

        private void DisplayHat(bool on)
        {
            foreach (Image hat in Hat)
            {
                hat.gameObject.SetActive(on);
            }
        }
        private void DisplayBadge(bool on)
        {
            Badge.gameObject.SetActive(on);
        }

    }
}