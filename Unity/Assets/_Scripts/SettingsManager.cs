﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace DivideEtImpera
{
    public class SettingsManager : MonoBehaviour
    {
        public CanvasGroup Info;

        void Start()
        {

        }

        public void OpenInfo()
        {
            if (Info.gameObject.activeSelf && Info.alpha > .9f)
            {
                CloseInfo();
                return;
            }

            Info.alpha = 0;
            Info.gameObject.SetActive(true);
            Info.DOFade(1, 0.5f);
        }

        public void CloseInfo()
        {
            Info.DOFade(0, 0.5f).OnComplete(() => { Info.gameObject.SetActive(false); });
        }

        public void Restart()
        {
            DOTween.KillAll();

            Destroy(DEIManager.I().gameObject);

            SimpleI18n.Language = Language.Undecided;

            SceneManager.LoadScene("Intro");
        }

        public void VisitMauro()
        {
            Application.OpenURL("https://maurovanetti.itch.io/");
        }

        public void VisitOpenLab()
        {
            Application.OpenURL("https://open-lab.com/");
        }

        public void VisitPYR()
        {
            Application.OpenURL("https://www.playyourrole.eu/");
        }

    }
}
