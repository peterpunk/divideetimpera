﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.DeExtensions;
using DG.Tweening;
using TMPro;
using System;
using DivideEtImpera;
using System.Collections.Generic;

public class GoalUpdate : MonoBehaviour
{
    public TextMeshProUGUI text;
    
    public void SetLargestBubble(int n)
    {
        int goal = 25 - DEIManager.I().Params.LargestBubbleSizeToWin;
        int result = 25 - n;
        Dictionary<Language, string> message = new Dictionary<Language, string>();

        if (result == 0)
        {
            message.Add(Language.En, "Sir, the community is united. Split it in half to win.");
            message.Add(Language.It, "Capo, la comunità è unita. La spacchi a metà per vincere.");
        }
        else if (result < goal)
        {
            message.Add(Language.En, $"Sir, you've split {result} people away (shown in gray).\nMake it {goal} to win.");
            message.Add(Language.It, $"Capo, ha diviso {result} persone (mostrate in grigio) dalle altre.\nSeparandone {goal} vincerà.");
        }
        else
        {
            message.Add(Language.En, $"Sir, you've managed to split {result} people away from their community!");
            message.Add(Language.It, $"Capo, è riuscito a dividere {result} persone dalla loro comunità!");
        }

        this.text.text = message[SimpleI18n.Language];
    }
}
