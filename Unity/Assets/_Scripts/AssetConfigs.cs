﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DivideEtImpera
{
    public class AssetConfigs : ScriptableObject
    {
        public List<PersonType> PersonTypes;

        public Sprite GetSpriteByName(string name)
        {
            foreach (var personType in PersonTypes)
            {
                if (personType.Name == name)
                    return personType.Face;
            }

            return null;
        }

        [Serializable]
        public class PersonType
        {
            public string Name;
            public Sprite Face;
        }
    }
}
