﻿using System.Collections;
using System.Collections.Generic;
using DG.DeExtensions;
using DG.Tweening;
using OL;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using System;

namespace DivideEtImpera
{

    public class PersonDrawer : MonoBehaviour
    {
        [SerializeField]
        Transform Detail;

        [SerializeField]
        CanvasGroup Cover;

        [SerializeField]
        Sprite Ghost;

        [SerializeField]
        Image Focused;
        [SerializeField]
        Transform North;
        [SerializeField]
        Transform South;
        [SerializeField]
        Transform East;
        [SerializeField]
        Transform West;

        [SerializeField]
        Image MainPortrait;

        [SerializeField]
        TextMeshProUGUI PersonalData;

        [SerializeField]
        TextMeshProUGUI RelationshipWithPlayer;

        [SerializeField]
        TextMeshProUGUI Logs;

        Person FocusedPerson;

        public static PersonDrawer I;


        void Awake()
        {
            I = this;
        }

        void Start()
        {
            Cover.alpha = 0;
            Detail.gameObject.SetActive(false);
        }

        public void OpenDetail(Person person)
        {
            FocusedPerson = person;

            Cover.alpha = 1;
            Detail.gameObject.SetActive(true);

            Focused.overrideSprite = person.Portrait.sprite;
            MainPortrait.overrideSprite = person.Portrait.sprite;

            UpdateHatAndBadge(person, Focused, TT.FirstImage, TT.SecondImage);

            UpdateInDirection(person, Person.Direction.NORTH, North, false);
            UpdateInDirection(person, Person.Direction.EAST, East, true);
            UpdateInDirection(person, Person.Direction.SOUTH, South, false);
            UpdateInDirection(person, Person.Direction.WEST, West, true);

            var myself = person.Myself;
            PersonalData.text =
                Translator._("$NAME_AGE_GENDER")
                .Replace("((NAME))", myself.Name).Replace("((AGE))", myself.Age).Replace("((GENDER))", myself.LocalizedGender)
                + '\n' +
                Translator._("$CITIZENSHIP").Replace("((CITIZENSHIP))", myself.LocalizedCitizenship)
                + '\n' +
                Translator._("$CREED").Replace("((CREED))", myself.LocalizedCreed);
            int ev = Mathf.RoundToInt(100 * myself.EngagementVariation());
            RelationshipWithPlayer.text = Translator._("$ENGAGEMENT_WITH_YOU");
            if (ev == 0)
            {
                RelationshipWithPlayer.text = RelationshipWithPlayer.text.Replace("((ENGAGEMENT))",
                    Translator._("$AVERAGE_ENGAGEMENT"));
            } 
            else
            {
                RelationshipWithPlayer.text = RelationshipWithPlayer.text.Replace("((ENGAGEMENT))", $"{(ev > 0 ? "+" : "")}{ev:0}% ");
            }


            int experiencesCount = myself.Experiences.Count;
            if (experiencesCount == 0)
            {
                Logs.text = "<i>" + Translator._("$NOT_YET_AFFECTED") + "</i>";
            }
            else
            {
                Logs.text = "<b><u>" + Translator._("$LAST_EFFECT") + "</u></b>\n" + myself.Experiences[experiencesCount - 1].Print();
                if (experiencesCount > 1)
                {
                    Logs.text += "\n--------------\n";
                    Logs.text += "<b><u>" + Translator._("$PREVIOUS_EFFECTS") + "</u></b>\n<size=75%>";
                    for (int i = myself.Experiences.Count - 2; i >= 0; i--)
                    {
                        Logs.text += myself.Experiences[i].Print();
                        Logs.text += "\n--------------\n";
                    }
                    Logs.text += "</size>";
                }
            }

            Cover.DOFade(0, .5f);
        }

        static void UpdateHatAndBadge(Person person, Image portrait, string hatTag, string badgeTag)
        {
            Tuple<bool, bool> hatAndBadge = person.HatAndBadge();
            List<GameObject> hats = TT.FindGameObjectsChildrenWithTag(portrait.gameObject, hatTag);            
            foreach (GameObject hat in hats)
            {
                hat.SetActive(hatAndBadge.Item1);
            }
            GameObject badge = TT.FindGameObjectChildWithTag(portrait.gameObject, badgeTag);
            badge.SetActive(hatAndBadge.Item2);
        }

        void UpdateInDirection(Person focusedPerson, Person.Direction direction, Transform inDirection, bool changeY)
        {
            var gameParams = DEIManager.I().Params;

            var neighbourPortrait = TT.FindGameObjectChildWithTag(inDirection.gameObject, TT.FirstImage).
                GetComponent<Image>();
            var relImage =
                TT.FindGameObjectChildWithTag(inDirection.gameObject, TT.SecondImage).GetComponent<Image>();
            var relDesc =
                TT.FindGameObjectChildWithTag(inDirection.gameObject, TT.FirstText).GetComponent<TextMeshProUGUI>();

            var neighbour = focusedPerson.GetNeighbour(direction);
            if (neighbour != null)
            {
                relImage.SetAlpha(1f);
                relDesc.gameObject.SetActive(true);
                neighbourPortrait.SetAlpha(1f);
                neighbourPortrait.overrideSprite = neighbour.Portrait.sprite;

                UpdateHatAndBadge(neighbour, neighbourPortrait, TT.ThirdImage, TT.FourthImage);                

                var neighbourRelationship = focusedPerson.Myself.NeighbourRelationships[direction];

                var relPerc = neighbourRelationship.Intensity / gameParams.InitialRelationshipMax;

                if (relPerc > 0.8f)
                {
                    relDesc.text = Translator._("$GOOD_RELATIONSHIP");
                }
                else if (relPerc > 0.3f)
                {
                    relDesc.text = Translator._("$OK_RELATIONSHIP");
                }
                else if (relPerc > 0.0f)
                {
                    relDesc.text = Translator._("$BAD_RELATIONSHIP");
                }
                else
                {
                    relDesc.text = Translator._("$BROKEN_RELATIONSHIP");
                }

                relDesc.text += $" ({(int)(relPerc * 100)}%)";
                
                DEIManager.I().AdjustRelationshipThickness(relImage, relPerc, changeY);
            }
            else
            {
                neighbourPortrait.overrideSprite = Ghost;
                var hat = TT.FindGameObjectChildWithTag(neighbourPortrait.gameObject, TT.ThirdImage).
                    GetComponent<Image>();
                hat.gameObject.SetActive(false);
                var badge = TT.FindGameObjectChildWithTag(neighbourPortrait.gameObject, TT.FourthImage).
                    GetComponent<Image>();
                badge.gameObject.SetActive(false);
                neighbourPortrait.SetAlpha(1f);
                relImage.SetAlpha(.3f);
                relDesc.gameObject.SetActive(false);
            }
        }

        private void RemoveThumb()
        {
            FocusedPerson.Flavor = Person.FlavorType.NEUTER;
            PostManager.I.CheckAheadButton();
        }

        public void CloseDetail()
        {
            Cover.DOFade(1, .5f).OnComplete(() => Detail.gameObject.SetActive(false));
            RemoveThumb();
        }

        public void ChangeDetail(int directionNum)
        {
            RemoveThumb();
            var direction = (Person.Direction)directionNum;
            var neighbour = FocusedPerson.GetNeighbour(direction);

            if (neighbour != null)
            {
                Cover.DOFade(.7f, .3f).OnComplete(() =>
                {
                    OpenDetail(neighbour);
                });
            }
        }
    }
}
