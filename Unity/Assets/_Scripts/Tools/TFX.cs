﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DG.DeAudio;
using DG.Tweening;
using TMPro;
using UnityEngine;
using static UnityEngine.Mathf;

namespace OL
{
    public static class TFX
    {
        public class RollToScoreConfig
        {
            public TMP_Text textComponent;
            public int start;
            public int end;
            public int step;
            public Action callback;
            public string numberpostfix;
            public AudioClip feedback;
        }

        public static void RollToScore(TMP_Text textComponent, int start, int end, int step, Action callback = null, string numberpostfix = null)
        {
            RollToScoreConfig rtsc = new RollToScoreConfig();
            rtsc.textComponent = textComponent;
            rtsc.start = start;
            rtsc.end = end;
            rtsc.step = step;
            rtsc.callback = callback;
            rtsc.numberpostfix = numberpostfix;

            RollToScore(rtsc);
        }

        public static void RollToScore(RollToScoreConfig rtsc)
        {
            rtsc.textComponent.text = rtsc.start + (string.IsNullOrEmpty(rtsc.numberpostfix) ? "" : rtsc.numberpostfix);
            RollToStep(rtsc);
        }

        static void RollToStep(RollToScoreConfig rtsc)
        {
            int score = rtsc.start;
            DOTween.To(() => score, x => score = x+(rtsc.step-1), rtsc.end, .1f * Mathf.Abs(rtsc.end - rtsc.start)).OnUpdate(() =>
            {
                var currenttext = score + (string.IsNullOrEmpty(rtsc.numberpostfix) ? "" : rtsc.numberpostfix);
                if (currenttext != rtsc.textComponent.text)
                {
                    rtsc.textComponent.text =
                        currenttext;
                    if (rtsc.feedback != null)
                        DeAudioManager.Play(rtsc.feedback);
                }
            }
                    )
                .SetEase(Ease.Linear).OnComplete(() =>
                {
                    if (rtsc.callback != null)
                        rtsc.callback();
                });
        }

        public enum CharEffect
        {
            TEXT_DROP,
        }
        
        /*
        using System.Collections;
using DG.Tweening;
using TMPro;
using UnityEngine;

public class TextMeshPro_CharAnimation : BrainBase
    {
        public float timeScale = 1;
        public float offsetDuration = 1;
        public float rotateDuration = 1;
        public float scaleDuration = 1;
        public float colorDuration = 1;
        public float fadeDuration = 1;
        public Gradient colorGradient;
        public TMP_Text tfMain, tfWSprites;

        IEnumerator Start()
        {
            // Increase tweens capacity because we're gonna create lots of nested tweens for all characters
            DOTween.SetTweensCapacity(1000, 50);

            // Create Sequences
            // ► Main text
            DOTweenTMPAnimator tmpMain = new DOTweenTMPAnimator(tfMain);
            // Shift vertices test
            for (int i = 24; i < 32; ++i)
            {
                float shiftY = i % 2 == 0 ? 10 : -10;
                tmpMain.ShiftCharVertices(i, new Vector3(0, shiftY, 0), new Vector3(0, 0, 0), new Vector3(0, shiftY, 0), new Vector3(0, 0, 0));
            }
            for (int i = 24; i < 32; ++i) tmpMain.SetCharOffset(i, new Vector3(0, i % 2 == 0 ? -4 : 4, 0));
            //        yield return new WaitForSeconds(2);
            //
            int charCount = tmpMain.textInfo.characterCount;
            Sequence sMain = DOTween.Sequence().Pause();
            sMain.timeScale = timeScale;
            for (int i = 0; i < charCount; ++i)
            {
                Vector3 currCharOffset = tmpMain.GetCharOffset(i);
                sMain.Insert(i * 0.1f, tmpMain.DOFadeChar(i, 0, fadeDuration).From());
                sMain.Insert(i * 0.1f, tmpMain.DOOffsetChar(i, currCharOffset + new Vector3(0, 30, 0), offsetDuration))
                    .Insert(i * 0.1f + offsetDuration, tmpMain.DOOffsetChar(i, currCharOffset + Vector3.zero, offsetDuration).SetEase(Ease.OutBack));
                sMain.Insert(i * 0.1f, tmpMain.DOScaleChar(i, new Vector3(2, 5, 2), scaleDuration).From());
                sMain.Insert(i * 0.1f, tmpMain.DORotateChar(i, new Vector3(-90, 60, 90), rotateDuration).From());
                sMain.Insert(2 + i * 0.1f, tmpMain.DOColorChar(i, colorGradient.Evaluate(1f / charCount * i), colorDuration));
                sMain.Insert(2 + i * 0.1f, tmpMain.DOPunchCharScale(i, 0.5f, colorDuration, 5));
                sMain.Insert(2 + i * 0.1f, tmpMain.DOPunchCharRotation(i, new Vector3(0, 0, 30), colorDuration, 5));
                sMain.Insert(2 + i * 0.1f, tmpMain.DOPunchCharOffset(i, new Vector3(0, 18f, 0), colorDuration, 5));
                int delay = 5;
                sMain.Insert(delay + i * 0.1f, tmpMain.DOShakeCharOffset(i, colorDuration, 30f));
                sMain.Insert(delay + 2f + i * 0.1f, tmpMain.DOShakeCharScale(i, colorDuration, 5f));
                sMain.Insert(delay + 4f + i * 0.1f, tmpMain.DOShakeCharRotation(i, colorDuration, new Vector3(180f, 180f, 180f)));
            }
            // ► Text with inline sprites
            DOTweenTMPAnimator tmpSprites = new DOTweenTMPAnimator(tfWSprites);
            charCount = tmpSprites.textInfo.characterCount;
            Sequence sSprites = DOTween.Sequence().Pause();
            for (int i = 0; i < charCount; ++i)
            {
                sSprites.Insert(i * 0.05f, tmpSprites.DOFadeChar(i, 0, 0.3f).From());
                sSprites.Insert(i * 0.05f, tmpSprites.DOScaleChar(i, 4f, 0.8f).From().SetEase(Ease.OutBack, 3));
            }
            yield return new WaitForSeconds(1);

            // Play all
            sMain.Play();
            sSprites.Play();
            yield return new WaitForSeconds(4);

            // Start looping single word in main textField
            for (int i = 24; i < 32; ++i)
            {
                LoopCharScale(tmpMain, i, 1.2f, i * 0.1f);
                tmpMain.DOColorChar(i, Color.yellow, 0.5f).SetEase(Ease.InOutSine).SetLoops(-1, LoopType.Yoyo).SetDelay(i * 0.1f);
            }
            // Start looping emoticons in inline sprites text
            LoopCharScale(tmpSprites, new[] { 8, 19, 35, 45, 54 }, 1.5f);
        }

        void LoopCharScale(DOTweenTMPAnimator animator, int[] charIndexes, float scale, float delay = 0)
        {
            for (int i = 0; i < charIndexes.Length; ++i) LoopCharScale(animator, charIndexes[i], scale, delay);
        }

        void LoopCharScale(DOTweenTMPAnimator animator, int charIndex, float scale, float delay = 0)
        {
            animator.DOScaleChar(charIndex, scale, 0.5f).SetEase(Ease.InOutSine).SetLoops(-1, LoopType.Yoyo).From(new Vector3(1, 1, 1)).SetDelay(delay);
        }
    }*/


    }
}
