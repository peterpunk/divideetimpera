﻿// Author: Pietro Polsinelli - http://designAGame.eu
// Twitter https://twitter.com/ppolsinelli
// License: WTFPL, all free as in free beer :-)
// Tested with Unity 5.6.1
// Created: 22 06 2017

using System.Collections;
using DG.DeAudio;
using DG.Tweening;

using UnityEngine;
using UnityEngine.SceneManagement;

namespace OL
{
#pragma warning disable 0649
    public class SceneChanger : MonoBehaviour
    {
        [SerializeField]
        public SpriteRenderer Cover;

        [SerializeField]
        Transform CoverTransform;

        [SerializeField]
        //float AudioFadeInTime = 1;

        Setuppable Setup;


        public static SceneChanger I;

        void Awake()
        {
            if (I == null)
            {
                I = this;
                if (Cover != null)
                {
                    Cover.gameObject.SetActive(true);
                }

                Setup = Setuppable.NewInstance();

                if (CoverTransform != null)
                {
                    //Fullscreen Sprite Background
                    TT.FitSpriteToCamera(CoverTransform, Cover);
                }
            }
        }

        void Start()
        {
            Cover.DOFade(0, .7f).OnComplete(() =>
            {
                Cover.gameObject.SetActive(false);
            });
        }

        /*void Update()
        {
            if (!SimpleGameManager.InjectedReferrableInstance.FrameworkAndPlayState.IsRunning())
                return;

            if (Setup.IsToBeSetupped())
            {
                Setup.SetupCompleted();
                Cover.DOFade(0, .7f).OnComplete(() =>
                {
                    Cover.gameObject.SetActive(false);
                });

                //DeAudioManager.GetAudioGroup(DeAudioGroupId.Ambient).volume = 1f;
                //DeAudioManager.GetAudioGroup(DeAudioGroupId.Custom0).volume = 1f;
                //DeAudioManager.GetAudioGroup(DeAudioGroupId.Custom1).volume = 1f;
                DOTween.To(() => DeAudioManager.globalVolume, x => DeAudioManager.globalVolume = x,
                SimpleGameManager.InjectedReferrableInstance.ReferenceMasterAudioVolume, AudioFadeInTime);
            }
        }*/

        public static void ChangeScene(string sceneName, bool doFdaOut = true)
        {
            DOTween.KillAll();

            if (doFdaOut)
                DeAudioManager.FadeOut(0.7f);

            I.Cover.gameObject.SetActive(true);
            I.Cover.DOFade(1, 0.75f).OnComplete(() =>
            {
                I.StartCoroutine(LoadSceneAsync(sceneName));
            });
        }

        static IEnumerator LoadSceneAsync(string sceneName)
        {
            AsyncOperation async = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Single);
            yield return async;
            Resources.UnloadUnusedAssets();
        }

        public static void DoTransitionInOut()
        {

            I.Cover.gameObject.SetActive(true);
            I.Cover.DOFade(1, 0.7f).OnComplete(() =>
            {
                I.Cover.DOFade(0, 0.7f).OnComplete(() => I.Cover.gameObject.SetActive(false));

            });
        }

        public static void DoTransitionIn()
        {

            I.Cover.gameObject.SetActive(true);
            I.Cover.DOFade(1, 0.7f);

        }

        public static void DoTransitionOut()
        {

            I.Cover.DOFade(0, 0.7f).OnComplete(() => I.Cover.gameObject.SetActive(false));

        }
    }
}