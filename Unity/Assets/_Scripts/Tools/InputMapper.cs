﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.De2D;
using DG.Tweening;

using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Rendering;
using UnityEngine.UI;

public class InputMapper : MonoBehaviour
{
    List<DeSpriteButton> WorldButtons;

    List<Button> UIButtons;

    List<Action> Callbacks;

    bool collectInputEnabled;

    //[Title("RUNTIME")]
    public ListenToEntity ListenTo;
    public bool AlsoCollectMouseUp;

    //[Title("DEBUG")]
    public DeSpriteButton Focused;
    public Button UIFocused;
    public int CallbackCount;
    public int WorldButtonsCount;
    public int RecentlyEnabledUIButtonsCount;
    public int RecentlyEnabledWorldButtonsCount;

    //other
    int focused;

    List<DeSpriteButton> recentlyEnabledWorldButtons;
    List<Button> recentlyEnabledUIButtons;
    public enum ListenToEntity
    {
        WORLD_BUTTONS,
        CALLBACKS,
        UI_BUTTONS
    }

    void Start()
    {
        recentlyEnabledWorldButtons = new List<DeSpriteButton>();
        recentlyEnabledUIButtons = new List<Button>();
        WorldButtons = new List<DeSpriteButton>();
        Callbacks = new List<Action>();
        UIButtons = new List<Button>();
        AlsoCollectMouseUp = false;
    }

    public void Setup(params DeSpriteButton[] worldButtons)
    {
        ListenTo = ListenToEntity.WORLD_BUTTONS;
        this.WorldButtons.Clear();
        this.WorldButtons.AddRange(worldButtons);
        UpdateEnabledButtons();
        Focused = recentlyEnabledWorldButtons[0];
        collectInputEnabled = true;
        UpdateInfo();
    }


    public void SetupUI(params Button[] uiButtons)
    {
        ListenTo = ListenToEntity.UI_BUTTONS;
        this.UIButtons.Clear();
        this.UIButtons.AddRange(uiButtons);
        UpdateEnabledUIButtons();
        collectInputEnabled = true;
        UpdateInfo();
    }

    public void SetupCallbacks(params Action[] callbacks)
    {
        ListenTo = ListenToEntity.CALLBACKS;
        this.Callbacks.Clear();
        this.Callbacks.AddRange(callbacks);
        UpdateEnabledButtons();
        collectInputEnabled = true;
        UpdateInfo();
    }

    public void UpdateEnabledButtons()
    {
        DeSpriteButton focusedNow = null;

        if (recentlyEnabledWorldButtons.Count > 0)
            focusedNow = recentlyEnabledWorldButtons[focused];

        recentlyEnabledWorldButtons.Clear();

        for (int i = 0; i < WorldButtons.Count; i++)
        {
            if (WorldButtons[i].interactable)
            {
                recentlyEnabledWorldButtons.Add(WorldButtons[i]);
            }
        }

        if (focusedNow != null)
        {
            for (int i = 0; i < recentlyEnabledWorldButtons.Count; i++)
            {
                if (recentlyEnabledWorldButtons[i] == focusedNow)
                {
                    focused = i;
                }
            }
        }

        UpdateInfo();
    }

    public void UpdateEnabledUIButtons()
    {
        Button focusedNow = null;

        if (recentlyEnabledUIButtons.Count > 0)
            focusedNow = recentlyEnabledUIButtons[focused];

        recentlyEnabledUIButtons.Clear();

        for (int i = 0; i < UIButtons.Count; i++)
        {
            if (UIButtons[i].interactable)
            {
                recentlyEnabledUIButtons.Add(UIButtons[i]);
            }
        }

        if (focusedNow != null)
        {
            for (int i = 0; i < recentlyEnabledUIButtons.Count; i++)
            {
                if (recentlyEnabledUIButtons[i] == focusedNow)
                {
                    focused = i;
                }
            }
        }

        UpdateInfo();
    }

    void UpdateInfo()
    {
        CallbackCount = Callbacks.Count;
        WorldButtonsCount = WorldButtons.Count;
        RecentlyEnabledWorldButtonsCount = recentlyEnabledWorldButtons.Count;
        RecentlyEnabledUIButtonsCount = recentlyEnabledUIButtons.Count;
    }

    bool justClicked;
    void Update()
    {
        if (!collectInputEnabled)
            return;

        if (justClicked)
            return;

        if (Callbacks.Count == 0 && recentlyEnabledWorldButtons.Count == 0)
            return;

        int count = 0;
        switch (ListenTo)
        {
            case ListenToEntity.CALLBACKS:
                count = Callbacks.Count;
                break;

            case ListenToEntity.UI_BUTTONS:
                count = recentlyEnabledUIButtons.Count;
                break;

            case ListenToEntity.WORLD_BUTTONS:
                count = recentlyEnabledWorldButtons.Count;
                break;
        }

        if (Input.GetKeyUp(KeyCode.Return) || (AlsoCollectMouseUp && Input.GetMouseButtonUp(0)))
        {
            switch (ListenTo)
            {
                case ListenToEntity.CALLBACKS:
                    Callbacks[focused]();
                    justClicked = true;
                    DOVirtual.DelayedCall(.3f, () => justClicked = false);
                    break;

                case ListenToEntity.UI_BUTTONS:
                    count = recentlyEnabledUIButtons.Count;
                    break;

                case ListenToEntity.WORLD_BUTTONS:
                    if (recentlyEnabledWorldButtons[focused].interactable)
                    {
                        recentlyEnabledWorldButtons[focused].onClick.Invoke();
                        justClicked = true;
                        DOVirtual.DelayedCall(.3f, () => justClicked = false);
                    }
                    break;
            }
        }
        else if (Input.GetKeyUp(KeyCode.UpArrow) || Input.GetKeyUp(KeyCode.DownArrow) ||
                  Input.GetKeyUp(KeyCode.LeftArrow) || Input.GetKeyUp(KeyCode.RightArrow))
        {
            //move focus
            if (Input.GetKeyUp(KeyCode.UpArrow) || Input.GetKeyUp(KeyCode.LeftArrow))
            {
                focused--;
                if (focused < 0)
                    focused = count - 1;
            }
            if (Input.GetKeyUp(KeyCode.DownArrow) || Input.GetKeyUp(KeyCode.RightArrow))
            {
                focused++;
                if (focused > count - 1)
                {
                    focused = 0;
                }
            }

            UpdateInfo();

            //show focused
            if (count > 0)
            {
                for (int i = 0; i < count; i++)
                {
                    switch (ListenTo)
                    {
                        case ListenToEntity.CALLBACKS:
                            break;

                        case ListenToEntity.UI_BUTTONS:
                            if (i == focused)
                            {
                                //recentlyEnabledUIButtons[i].onEnter.Invoke();
                                UIFocused = recentlyEnabledUIButtons[i];
                                EventSystem.current.SetSelectedGameObject(UIFocused.gameObject);
                            }
                            //else
                            //    recentlyEnabledWorldButtons[i].onExit.Invoke();
                            break;

                        case ListenToEntity.WORLD_BUTTONS:
                            if (i == focused)
                            {
                                recentlyEnabledWorldButtons[i].onEnter.Invoke();
                                Focused = recentlyEnabledWorldButtons[i];
                            }
                            else
                                recentlyEnabledWorldButtons[i].onExit.Invoke();
                            break;
                    }
                }
            }
        }
    }

    public void FocusOnUI(Transform uiObject, bool disableWorldButtons)
    {
        if (disableWorldButtons)
        {
            foreach (var deSpriteButton in recentlyEnabledWorldButtons)
            {
                deSpriteButton.interactable = false;
            }
        }
        collectInputEnabled = false;
        EventSystem.current.SetSelectedGameObject(uiObject.gameObject);
    }

    public void EnableWorldInput(float delay)
    {
        DOVirtual.DelayedCall(delay, () =>
        {
            foreach (var deSpriteButton in recentlyEnabledWorldButtons)
            {
                deSpriteButton.interactable = true;
            }

            collectInputEnabled = true;
        });

    }

    public void FocusOnButton(DeSpriteButton deSpriteButton)
    {
        for (int i = 0; i < recentlyEnabledWorldButtons.Count; i++)
        {
            if (deSpriteButton == recentlyEnabledWorldButtons[i])
            {
                focused = i;
                recentlyEnabledWorldButtons[i].onEnter.Invoke();
                Focused = recentlyEnabledWorldButtons[i];
            }
            else
                recentlyEnabledWorldButtons[i].onExit.Invoke();
        }
    }

    public void ResetWorldFocusUI()
    {
        Callbacks.Clear();
        //ListenTo = ListenToEntity.UI_BUTTONS;
        if (recentlyEnabledUIButtons != null && recentlyEnabledUIButtons.Count > 0)
            UIFocused = recentlyEnabledUIButtons[0];
        focused = 0;
        collectInputEnabled = false;
        if (UIFocused != null)
            EventSystem.current.SetSelectedGameObject(UIFocused.gameObject);
    }
}
