﻿using UnityEngine;
using UnityEditor;
using System;
using System.Collections.Generic;

public enum Language
{
    Undecided,
    En, // English
    It  // Italian
}

public class SimpleI18n
{
    public static List<Language> AllLanguages = new List<Language>() { Language.En, Language.It };
    public static Language Language = Language.Undecided;
}