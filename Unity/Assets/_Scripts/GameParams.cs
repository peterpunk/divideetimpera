﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DivideEtImpera
{
    public class GameParams : ScriptableObject
    {
        public float InitialRelationshipMin;
        public float InitialRelationshipMax;

        public int InitialEngagementMin;
        public int InitialEngagementMax;

        // network relation intensity
        public float IntensityChangeAmplifier;

        // UI

        public float MaxThicknessRelInDetail;
        public float MaxThicknessRelInGrid;

        // trust
        public float MinPercToActivateHat;
        public float MinPercToActivateBadge;

        // tot number of posts
        public int LargestBubbleSizeToWin;

        public int MinMovesForBadOutcome;
             

        // palatability factor for offensive posts within the Overton window
        public float OvertonOffensivePalatabilityFactor;
        public float OvertonPatalabilityMainThemeStretch;
        public float OvertonPatalabilitySubthemeStretch;

        public int TurnsToRestoreConfidence;

    }
}
